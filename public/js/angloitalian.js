
$(document).ready(function(){

    GetBagCount();

    DeleteProductFromBag();

    FollowAccordion();

    changePassSubmit();
    
    signUpSubmit();

    signInSubmit();

    validatePasswordConfirmation();

    validateEmailInputs('#emailaddress, #emailaddresssignup');

    validatePasswordInputs('#passwordsignin, #passwordsignup, #oldpass, #newpass, #newpassconf');

    validateSimpleInputs('#firstname, #lastname, #answerInput');

    validateStreetAddress('#streetaddress, #streetaddress2');

    validatePhoneNumber('#phonenumber');

    validateSimpleNumericInputs('#postalCode');

    DecreaseQuantity();

    IncreaseQuantity();
    
    SearchMethod();
    
    ProductOverlay();
    
    ProductRedirect();
    
    CategoryFiltering();
    
    CrudEditFunction();
    
    ZoomFunction();
    
    SizeIndicatorCircle();
    
    AddToBag();
    
    $(window).on('resize', function(){
        ClickCircle();
    });
    
});


//Validate all necessary inputs for password change in section
function changePassSubmit(){
    $('#changepass').click(function(e){
        console.log('clicked');
        // e.preventDefault();
        $('.signin-error-messages').fadeIn();
        var passwordValue = $('#oldpass').val().toString();
        var newpasswordvalue = $('#newpass').val().toString();
        var newpasswordconfvalue = $('#newpassconf').val().toString();

        if(!validatePassword(passwordValue) || !validatePassword(newpasswordvalue) || !validatePasswordConfirmation(newpasswordconfvalue) || passwordValue === newpasswordvalue || passwordValue.length == 0 || newpasswordvalue.length == 0 || newpasswordconfvalue.length == 0){
            var errorMessage = ChangePasswordMessage(passwordValue, newpasswordvalue, newpasswordconfvalue);
            
            if(errorMessage != 1){
                e.preventDefault();
                $('.signin-error-messages').html(errorMessage);
                setTimeout(function(){
                    $('.signin-error-messages').fadeOut();
                }, 2000);
            }
            
        }else{
            console.log('submit');
        }
    });
}


//Validate all necessary inputs for sign in section
function signInSubmit(){
    $('#signin').click(function(e){
        $('.signin-error-messages').fadeIn();
        var emailValue = $('#emailaddress').val().toString();
        var passwordValue = $('#passwordsignin').val().toString();
        if(!validateEmail(emailValue) && !validatePassword(passwordValue)){
            e.preventDefault();
            var errorMessage = signInErrorMessage(emailValue, passwordValue);
            $('.signin-error-messages').html(errorMessage);
            setTimeout(function(){
                $('.signin-error-messages').fadeOut();
                $('#signinemailalert').fadeOut();
                $('#signinpasswordalert').fadeOut();
            }, 2000);
        }else if(!validateEmail(emailValue) || !validatePassword(passwordValue)){
            e.preventDefault();
            var errorMessage = signInErrorMessage(emailValue, passwordValue);
            $('.signin-error-messages').html(errorMessage);
            setTimeout(function(){
                $('.signin-error-messages').fadeOut();
                $('#signinemailalert').fadeOut();
                $('#signinpasswordalert').fadeOut();
            }, 2000);
        }
    });
}

//Sign In Error Messages
function signInErrorMessage(emailValue, passwordValue){
    var errorMessage = `
            <div class="uk-alert uk-alert-danger">
                <p>  </p>
            </div>
        `;
    if(!validateEmail(emailValue)){
        errorMessage = `
            <div class="uk-alert uk-alert-danger">
                <p id="signinemailalert"> Enter a valid <strong>E-Mail</strong> Address </p>
            </div>
        `
    }
    if(!validatePassword(passwordValue)){
        errorMessage = `
            <div class="uk-alert uk-alert-danger">
                <p id="signinpasswordalert"> Enter a valid <strong>Password</strong> </p>
            </div>
        `
    }
    if(!validateEmail(emailValue) && !validatePassword(passwordValue)){
        errorMessage = `
            <div class="uk-alert uk-alert-danger">
                <p id="signinemailalert"> Enter a valid <strong>E-Mail</strong> Address </p>
                <p id="signinpasswordalert"> Enter a valid <strong>Password</strong> </p>
            </div>
        `
    }

    return errorMessage;
}

//Sign In Error Messages
function ChangePasswordMessage(passwordValue, newPassword, newPasswordConf){
    var errorMessage = `
            <div class="uk-alert uk-alert-danger">
        `;
    if(!validatePassword(passwordValue)){
        errorMessage += `
            <p id="signinpasswordalert"> Enter a valid <strong>Password</strong> </p>
        `
    }
    if(!validatePassword(newPassword)){
        errorMessage += `
            <p id="signinpasswordalert"> Enter a New <strong>Password</strong> </p>
        `
    }
    if(!validatePassword(newPasswordConf) && newPassword !== newPasswordConf){
        errorMessage += `
            <p id="signinpasswordalert"> Please confirm your <strong>Password</strong> </p>
        `
    }
    if(newPassword !== newPasswordConf){
        errorMessage += `
            <p id="signinpasswordalert"> Please confirm your <strong>Password</strong> </p>
        `
    }
    if(newPassword === passwordValue){
        errorMessage += `
            <p id="signinpasswordalert"> The new <strong>Password</strong> is the same as the old one </p>
        `
    }

    if(validatePassword(passwordValue) && validatePassword(newPassword) && validatePassword(newPasswordConf) && newPassword === newPasswordConf && newPassword !== passwordValue){
        return 1;
    }
    errorMessage += `
        </div>
    `

    return errorMessage;
}

//Validate all necessary inputs for sign in section
function signUpSubmit(){
    $('#signup').click(function(e){
        // e.preventDefault();
        var firstNameValue = $('#firstname').val().toString();
        var lastNameValue = $('#lastname').val().toString();
        var emailValue = $('#emailaddresssignup').val().toString();
        var streetAddress = $('#streetaddress').val().toString();
        var streetAddress2 = $('#streetaddress2').val().toString();
        var phoneNumber = $('#phonenumber').val().toString();
        var postalCode = $('#postalCode').val().toString();
        var passwordValue = $('#passwordsignup').val().toString();
        var passwordConfValue = $('#passwordconfsignup').val().toString();
        var answerValue = $('#answerInput').val().toString();
        if(validateName(firstNameValue) && validateName(lastNameValue) && validateEmail(emailValue) && validateAddress(streetAddress) && validateAddress(streetAddress2) && validatePhone(phoneNumber) && validateNumeric(postalCode) && validatePassword(passwordValue) && confirmPassword(passwordValue, passwordConfValue) && validateName(answerValue)){
            // window.location.href = 'user/sign';
        }else{
            e.preventDefault();
            signUpErrorMessages(firstNameValue, lastNameValue, emailValue, streetAddress, streetAddress2, phoneNumber, postalCode, passwordValue, passwordConfValue, answerValue);
        }
    });
}

//Sing Up Error Messgaes Handler
function signUpErrorMessages(firstNameValue, lastNameValue, emailValue, streetAddress, streetAddress2, phoneNumber, postalCode, passwordValue, passwordConfValue, answerValue){
    $('.dangerMessage').removeClass("uk-hidden");
    if((!validateName(firstNameValue) && firstNameValue != '') || firstNameValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="firstNameAlert">Enter a valid <strong>First Name</strong></p>
        `);
        setTimeout(function(){
            $('#firstNameAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#firstNameAlert').remove();
        }, 1500);
    }
    if((!validateName(lastNameValue) && lastNameValue != '') || lastNameValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id='lastNameAlert'>Enter a valid <strong>Last Name</strong></p>
        `);
        setTimeout(function(){
            $('#lastNameAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#lastNameAlert').remove();
        }, 1500);
    }
    if((!validateEmail(emailValue) && emailValue != '') || emailValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="emailAlert">Enter a valid <strong>Email Address</strong></p>
        `);
        setTimeout(function(){
            $('#emailAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#emailAlert').remove();
        }, 1500);
    }
    if((!validateAddress(streetAddress) && streetAddress != '') || streetAddress == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="addressAlert">Enter a valid <strong>Street Address</strong></p>
        `);
        setTimeout(function(){
            $('#addressAlert').fadeOut();
            $('.addressAlert').fadeOut();
            $('#addressAlert').remove();
        }, 1500);
    }
    if((!validateAddress(streetAddress2) && streetAddress2 != '') || streetAddress2 == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="addressAlert2">Enter a valid <strong>Street Address 2</strong></p>
        `);
        setTimeout(function(){
            $('#addressAlert2').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#addressAlert2').remove();
        }, 1500);
    }
    if((!validatePhone(phoneNumber) && phoneNumber != '') || phoneNumber == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="PhoneAlert">Enter a valid <strong>Phone Number</strong></p>
        `);
        setTimeout(function(){
            $('#PhoneAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#PhoneAlert').remove();
        }, 1500);
    }
    if((!validateNumeric(postalCode) && postalCode != '') || postalCode == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="postalCodeAlert">Enter a valid <strong>Postal Code</strong></p>
        `);
        setTimeout(function(){
            $('#postalCodeAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#postalCodeAlert').remove();
        }, 1500);
    }
    if((!validatePassword(passwordValue) && passwordValue != '') || passwordValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="passwordAlert">Enter a valid <strong>Password</strong></p>
        `);
        setTimeout(function(){
            $('#passwordAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#passwordAlert').remove();
        }, 1500);
    }
    if((!confirmPassword(passwordValue, passwordConfValue) && !validatePassword(passwordConfValue)  && passwordConfValue != '') || passwordConfValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="passwordConfAlert"><strong>Passwords</strong> aren't Identical</p>
        `);
        setTimeout(function(){
            $('#passwordConfAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#passwordConfAlert').remove();
        }, 1500);
    }
    if((!validateName(answerValue) && answerValue != '') || answerValue == ''){
        $('.dangerMessage').fadeIn().append(`
            <p id="answerAlert">Enter a valid <strong>Answer</strong></p>
        `);
        setTimeout(function(){
            $('#answerAlert').fadeOut();
            $('.dangerMessage').fadeOut();
            $('#answerAlert').remove();
        }, 1500);
    }
}

//Validate Password Confirmation
function validatePasswordConfirmation(){
    $('#passwordconfsignup').keyup(function(){
        var passwordValue = $('#passwordsignup').val().toString();
        var passwordConfirmation = $(this).val().toString();
        confirmPassword(passwordValue, passwordConfirmation);
    });
    $('#newpassconf').keyup(function(){
        var passwordValue = $('#newpassconf').val().toString();
        var passwordConfirmation = $(this).val().toString();
        confirmPassword(passwordValue, passwordConfirmation);
    });
}

//Validate Email inputs
function validateEmailInputs(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validateEmail(value)){
                $('#firstNameAlert').fadeOut();
                $(this).css('border', '1px solid #2ecc71');
                return true;
            }else{
                $(this).css('border', '1px solid #e74c3c');
                return false;
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Validate Password inputs
function validatePasswordInputs(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validatePassword(value)){
                $(this).css('border', '1px solid #2ecc71');
                return true;
            }else{
                $(this).css('border', '1px solid #e74c3c');
                return false;
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Validate Street Address
function validateStreetAddress(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validateAddress(value)){
                $(this).css('border', '1px solid #2ecc71');
                return true;
            }else{
                $(this).css('border', '1px solid #e74c3c');
                return false;
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Validate Simple Inputs
function validateSimpleInputs(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validateName(value)){
                $(this).css('border', '1px solid #2ecc71');
            }else{
                $(this).css('border', '1px solid #e74c3c');
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Validate Simple Inputs
function validateSimpleNumericInputs(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validateNumeric(value)){
                $(this).css('border', '1px solid #2ecc71');
            }else{
                $(this).css('border', '1px solid #e74c3c');
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Validate Simple Inputs
function validatePhoneNumber(selector){
    $(selector).keyup(function(){
        var value = $(this).val().toString();
        if(value != ''){
            if(validatePhone(value)){
                $(this).css('border', '1px solid #2ecc71');
            }else{
                $(this).css('border', '1px solid #e74c3c');
            }
        }else{
            $(this).css('border', '1px solid #e5e5e5');
        }
    });
}

//Accordion on top of the navbar 
function FollowAccordion(){
    $('#follow').click(function(e){
        e.preventDefault();
        UIkit.util.on('.acco', 'shown', function(){
            ClickCircle();
        });
        UIkit.util.on('.acco', 'hidden', function(){
            ClickCircle();
        });
        UIkit.accordion('.acco').toggle(0, true);
    });
}

//ProductOverlay that shows product infos on mouseover
function ProductOverlay(){
    
    //Product Infos Fade In
    $('.product').mouseover(function(e){
        var $overlay = $(this).children('.color-overlay');
        $overlay.css({'transition':'all 0.15s', 'opacity' : '1'});
    });
    
    //Product Infos Fade Out
    $('.product').mouseleave(function(){
        var $overlay = $(this).children('.color-overlay');
        $overlay.css({'transition':'all 0.15s', 'opacity' : '0'});
    });
    
}

//Search Method that uses AJAX for real time products fetching
function SearchMethod(){
    $('.searchInput').keyup(function(){
        if($('.searchInput').val() != ''){
            var keyword = $('.searchInput').val();
            $.post("../ajax/SearchEngine.php", {
                keyword: keyword
            }, function(data, status){
                $('.resultsContainer').html(data);
            });
        }else{
            $('.resultsContainer').html(``);
        }
    });
}

//The function that Redirect the user to the selected product
function ProductRedirect(){
    $('section.product').click(function(e){
        e.preventDefault();
        var id = $(this).children('.id').text();
        var overlay = $(this).children('.color-overlay');
        var prodNameParent = overlay.children('h5');
        var prodName = prodNameParent.children('#ProdName').text().toLowerCase().trim().replace('"', '!').replace('-', '_').replace(/\s/g, '-');
        
        window.location.href = "shop/product/" + prodName;
    });
}

//The function that redirect the user to the selected category
function CategoryFiltering(){
    
    var categ = (window.location.href).split('/');
    categ = categ[categ.length - 1];


    $('.categ').click(function(e){
        e.preventDefault();
        
        window.location.href = "/shop/home/" + $(this).text().toLowerCase();
        categ = $(this).text().toLowerCase();
    });
}

//The function that zoom product picture on mouse over
function ZoomFunction(){
    
    var imageSrc;
    $('.zoom').mouseover(function(){
        imageSrc = $(this).children('.productImages').attr('src');
    });
    $('.zoom').zoom({url : imageSrc, duration : 60}); 
    
    $('.zoom').click(function(){
        UIkit.lightbox('.productLightbox').show(0);
    });
}

//Selected Size Indicator on size select
function SizeIndicatorCircle(){
    $('.sizes .unavailable').click(function(e){
        e.preventDefault();
    })
    var firstSize = document.querySelector('.size:not(.unavailable)');
    $('.sizes .size:not(.unavailable)').click(function(e){
        e.preventDefault();
        $('.sizes li a').removeClass('selectedSize');
        $(this).addClass('selectedSize');
        var left = $(this).position().left;
        var top = $(this).position().top;
        $(".circle").removeClass('uk-hidden')
                    .css({transition: 'all 0.2s' ,top: top - 9, left: left - 11, position:'absolute'});
    });
    if(firstSize != null){
        firstSize.click();
    }
    
}

//Crud pencil icon that opens the edit modal
function CrudEditFunction(){
    $('a[uk-icon="pencil"]').click(function(e){
        e.preventDefault();
        var parentTR = $(this).parents('td').parents('tr');
        var productID = parentTR.find('.Id');
        var productName = productID.next();
        var productPrice = productName.next();

        productPrice = productPrice.text();
        productName = productName.text();

        productPrice = productPrice.substr(1);
        productPrice = productPrice.split(' ')[0];

        $('#EditProductId').val(productID.text());
        $('#EditProductName').val(productName);
        $('#EditProductPrice').val(productPrice);

    });
}

//Add to Bag function
function AddToBag(){
    $('#addtobag').click(function(e){
        e.preventDefault();
        var prodName = $('#ProdName').text();
        var selectedSize = $('.selectedSize').text();
        selectedSize = (selectedSize == '') ? 'Standard Size' : selectedSize;

        $.post('../ajax/AddToBag.php', {
            name: prodName,
            size: selectedSize
        }, function(data, status){
            if(status == 'success'){
                if(data == '1'){
                    UIkit.notification({message: 'This Product Already exists in your bag', pos: 'top-center'});
                    $('.uk-notification-message').css({'background-color':'#1c1e3b', 'color':'white', 'box-shadow':'1px 3px 6px #00000033'});
                }else if(data == '0'){
                    UIkit.notification({message: 'This Product was successfully Added to your bag' , pos: 'top-center'});
                    $('.uk-notification-message').css({'background-color':'#1c1e3b', 'color':'white', 'box-shadow':'1px 3px 6px #00000033'});
                    $('#bag').text('Bag ['+ (parseInt(GetBagCount()) + 1) +']');
                }else if(data == '-1'){
                    UIkit.notification({message: 'Please Sign in/up to get access to your bag', pos: 'top-center'});
                    $('.uk-notification-message').css({'background-color':'#1c1e3b', 'color':'white', 'box-shadow':'1px 3px 6px #00000033'});
                }else{
                    UIkit.notification({message: data, pos: 'top-center'});
                    $('.uk-notification-message').css({'background-color':'#1c1e3b', 'color':'white', 'box-shadow':'1px 3px 6px #00000033'});
                }
            }
        });
    });
}

//The function that Increases the quantoty of the product in the bag
function IncreaseQuantity(){
    $('.prod #plus').click(function(){
        var parent = $(this).parents('.prod');
        var name = parent.children('#bagProductName').text().toLowerCase();
        var size = parseInt(parent.children('#bagProductSize').text().toLowerCase());
        var quantity = parent.children('p').children('span.quantity').text();
        var plus = '+';
        
        $.post('../ajax/QuantityChanger.php', {
            name: name,
            size: size,
            quantity: quantity,
            plus: plus
        }, function(data, status){
            if(status == 'success'){
                $('body').load(data);
            }
        });
    });
}

//The function that Decreases the quantoty of the product in the bag
function DecreaseQuantity(){
    $('.prod #minus').click(function(){
        var parent = $(this).parents('.prod');
        var name = parent.children('#bagProductName').text().toLowerCase();
        var size = parseInt(parent.children('#bagProductSize').text().toLowerCase());
        var quantity = parent.children('p').children('span.quantity').text();
        var minus = '-';
        
        $.post('../ajax/QuantityChanger.php', {
            name: name,
            size: size,
            quantity: quantity,
            minus: minus
        }, function(data, status){
            if(status == 'success'){
                $('body').load(data);
            }
        });
    });
}

//Get a good price look i.e 135 --> 135.00 | 1200.98 --> 1,200.98
function PrettifyPrice(price){
    var priceParts = price.toString().split('.');
    var firstPart = priceParts[0];
    if(firstPart.length > 3){
        firstPart = firstPart.toString().substr(0, 1) + ',' + firstPart.toString().substr(1);
    }
    if(priceParts.length > 1){
        var secondPart = priceParts[1].toString();
        if(secondPart.toString().length > 1){
            return firstPart.toString() + '.' + secondPart.toString();
        }else{
            return firstPart.toString() + '.' + secondPart.toString() + '0'; 
        }
    }else{
        return firstPart.toString() + '.00';
    }
}

//Validate Phone Number
function validatePhone(value){
    var regexPhone = /^\+[0-9\- ]{5,17}$/g;
    return (value.match(regexPhone)) ? true : false;
}

//Validate E-Mails
function validateEmail(value){
    var emailRegex = /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9]{2,}\.[a-zA-Z]{2,5}/g;
    return (value.match(emailRegex)) ? true : false;
}

//Validate Passwords
function validatePassword(value){
    var passwordRegex = /^[a-zA-Z0-9-_/\\%]{5,20}$/g;
    return (value.match(passwordRegex)) ? true : false;
}

//Validate Simple Inputs
function validateName(value){
    var passwordRegex = /^[a-zA-Z ]{2,20}$/g;
    return (value.match(passwordRegex)) ? true : false;
}

//Validate Simple Inputs
function validateNumeric(value){
    var numericRegex = /^[a-zA-Z0-9]{1,15}$/g;
    return (value.match(numericRegex)) ? true : false;
}

//Validate Street Address
function validateAddress(value){
    var regexAddress = /^[a-zA-Z0-9\s,'-]*$/g;
    return (value.match(regexAddress)) ? true : false;
}

//Validate password confirmation
function confirmPassword(password, confirmation){
    if(confirmation != ''){
        if(password === confirmation){
            $('#passwordconfsignup').css('border', '1px solid #2ecc71');
            return true;
        }else{
            $('#passwordconfsignup').css('border', '1px solid #e74c3c');
            return false;
        }
    }else{
        $('#passwordconfsignup').css('border', '1px solid #e5e5e5');
    }
}

//Click the size indicator
function ClickCircle(){
    var firstSize = document.querySelector('.selectedSize');
    if(firstSize != null){
        firstSize.click();
    }
}

//Delete Product From Bag
function DeleteProductFromBag(){
    $('.prod #trashIcon').click(function(){
        var parent = $(this).parents('.prod');
        var name = parent.children('#bagProductName').text().toLowerCase();
        var size = parent.children('#bagProductSize').text().toLowerCase();
        
        $.post('../ajax/DeleteProductFromCart.php', {
            name: name,
            size: size
        }, function(data, status){
            if(status == 'success'){
                $('body').load(data);
                setTimeout(function(){
                    UIkit.notification({message: 'The Product was successfully removed from your Bag', pos: 'top-center'});
                $('.uk-notification-message').css({'background-color':'#1c1e3b', 'color':'white', 'box-shadow':'1px 3px 6px #00000033'});
                }, 300);
            }
        });
    });
}

//Get Bag Count from bag section in navbar
function GetBagCount(){
    var text = $('#bag').text();
    var count = text.substr(5);
    count = count.substr(0, count.length - 1);
    return count;
}

//Get Bag Count from bag section in navbar
function GetBagCountFromBag(){
    var count = $('.bagProducts .prod').length;
    var quantityCount = 0;
    for(var i = 1; i <= count; i++){
        var actQuantity = parseInt($('.bagProducts .prod:nth-of-type('+ i +')').children('#quantityManager').children('.quantity').text());
        quantityCount += actQuantity;
    }
    return quantityCount;
}