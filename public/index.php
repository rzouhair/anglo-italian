<?php 
    namespace ANGIT;

use ANGIT\LIB\FrontController;
use ANGIT\Models\ShopModel;

    include('../app/config.php');
    include('..' .DS. 'app' .DS. 'lib' .DS. 'autoload.php');
    
    $frontcontroller = new FrontController;
    $frontcontroller->dispatch();
?>