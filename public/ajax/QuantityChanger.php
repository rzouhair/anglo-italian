<?php

use ANGIT\Models\UserModel;
use ANGIT\Models\ShopModel;
include('../../app/models/abstractmodel.php');
include('../../app/models/shopmodel.php');
include('../../app/models/usermodel.php');

    if(isset($_POST['name']) && isset($_POST['size']) && isset($_POST['quantity']) && (isset($_POST['plus']) || isset($_POST['minus']))){
        $userInstance = new UserModel;
        $shopInstance = new ShopModel;
        
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }
        
        $userID = $_SESSION['SignedInUserID'];
        $cartID = $userInstance->getCartIDbyUserID($userID);
        $name = $_POST['name'];
        $ProducID = $shopInstance->getProductIdByName($name);
        $size = $_POST['size'];
        $quantity = $_POST['quantity'];
        
        if(isset($_POST['plus'])){
            $Done = $userInstance->IncreaseQuantity($cartID, $ProducID, $size);
        }else if(isset($_POST['minus'])){
            $Done = $userInstance->DecreaseQuantity($cartID, $ProducID, $size);
        }
        
        ?>
        <?php if(isset($ProductsInBag)) :
            foreach($ProductsInBag as $key => $product) : 
                $thisProducts = $shopInstance->getProductByID($product['ProductID'])[0];
                $image = $shopInstance->getPhotos($thisProducts['Id']);
                ?>
                
                <div class="prod">
                    <img src="../images/<?php echo $image[0] ?>" alt="prod">
                    <p id="bagProductName"><?php echo $thisProducts['Name'] ?></p>
                    <p id="bagProductSize"><?php echo $product['ProductSize'] ?></p>
                    <p>
                        <span id="minus">-</span>
                        &nbsp;&nbsp;&nbsp; <span class="quantity"><?php echo $product['ProductQuantity'] ?></span> &nbsp;&nbsp;&nbsp;
                        <span id="plus">+</span>
                    </p>
                    <p id="Price">£ <span id="priceText"><?php echo $shopInstance->prettifyPrice($thisProducts['Price'] * $product['ProductQuantity']) ?></span> <small>INC. TAX</small> </p>
                </div>
        <?php
                endforeach;
                ?>
                <!-- Total Section -->
                <section class="bagProductsTotal">
                    <div class="prodTotal">
                        <h3>Total</h3>
                        <h3 class="uk-margin-remove uk-padding-remove uk-text-success uk-text-bold">£ <span id="totalPriceText"><?php echo($shopInstance->prettifyPrice($userInstance->getTotalCartPrice($UserID))) ?></span> <small> <small>INC. TAX</small> </small> </h3>
                    </div>
                </section>
                <!-- ./Total Section -->

                <section class="bagButtons uk-visible@s uk-text-center">
                    <form action="#" method="post">
                        <input type="submit" value="CONTINUE SHOPPING">
                        <input type="submit" value="CHECK OUT">
                        <input type="submit" value="">
                    </form>
                </section>
                <section class="uk-visible uk-hidden@s bagButtons2 uk-width-1-1 uk-text-center">
                    <form action="#" method="post" class="">
                        <input class="uk-width-1-1" type="submit" value="CONTINUE SHOPPING">
                        <input class="uk-width-1-1" type="submit" value="CHECK OUT">
                        <input class="uk-width-1-1" type="submit" value="">
                    </form>
                </section>
                <?php
            endif;
        ?>
    <?php 

    }
?>
