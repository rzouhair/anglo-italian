<?php 
use ANGIT\Models\ShopModel;
include('../../app/models/abstractmodel.php');
include('../../app/models/shopmodel.php');

$Instance = new ShopModel;

if(isset($_POST['keyword'])){
    $keyword = $_POST['keyword'];
    $searchResult = $Instance->searchProducts($keyword);
    if(!empty($searchResult)){
        foreach($searchResult as $product){
            $prodName = str_replace('"', '!', $product['Name']);
            $prodName = str_replace('-', '_', $prodName);
            $prodName = str_replace(' ', '-', $prodName);
            
            echo '<section class="productModal uk-text-center">
                <a href="shop/product/'. strtolower($prodName) .'">
                    <img class="uk-height-max-medium uk-margin-large-top ProductResult" src="../images/'. $Instance->ParsePhotosRecord($product['Photos'])[0] .'" alt="Photo">
                    <h5>' . strtoupper($product['Name']) . '<br>£' . $product['Price'] . ' inc.tax</h5>
                </a>
            </section>';
        }
    }else{
        if(strlen(substr($keyword, 0, 10)) == 10){
            $subKeyword = substr($keyword, 0, 10) . '...';
        }else{
            $subKeyword = substr($keyword, 0, 10);
        }
        echo '<section class="productModal uk-text-center">
                <h1 class="uk-margin-large-top" style="color: white">
                    Sorry We Couldn\'t Find any result Matching '. $subKeyword .'
                </h1>
            </section>
            ';
    }
}else{
    ?>
    <script>
        window.location.href = "/notfoundpage";
    </script>
    <?php
}

?>