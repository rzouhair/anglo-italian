<?php 
use ANGIT\Models\ShopModel;
use ANGIT\Models\UserModel;
include('../../app/models/abstractmodel.php');
include('../../app/models/shopmodel.php');
include('../../app/models/usermodel.php');

$ShopInstance = new ShopModel;
$UserInstance = new UserModel;

if(isset($_POST['name']) && isset($_POST['size'])){
    $productName = $_POST['name'];
    $selectedSize = $_POST['size'];

    if(session_status() == PHP_SESSION_NONE){
        session_start(); 
    }
    if(isset($_SESSION['isSignedIn'])){
        $product = $ShopInstance->getProductByName($productName);
        if(!empty($product)){
            $ProductID = $product[0]['Id'];
            $UserID = $_SESSION['SignedInUserID'];
            $isProductInBag = $UserInstance->checkProductInBag($ProductID, $selectedSize, $UserID);
            if($isProductInBag == 1){
                echo '1';
            }else if($isProductInBag == 0){
                echo '0';
                $UserInstance->addToBag($UserID, $ProductID, $selectedSize);
            }else{
                echo '-1';
            }
            
        }else{
            echo ('This Product Doesn\'t exist');
        }
    }else{
        echo '-1';
    }
}else{
    ?>
    <script>
        window.location.href = "/notfoundpage";
    </script>
    <?php 
}

?>