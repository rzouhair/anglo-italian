<?php 
    if(!isset($_SESSION['isSignedIn'])){
?>
    <script>
        location.href = '/user/join'
    </script>
<?php
    }

    
    $message = 'There was an error';
    $messageType = 'error';
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    
    if (!empty($_GET)) {
        $_SESSION['got'] = $_GET;
        ?>
        <script>
            window.location.href = "user/changePassword/";
        </script>
        <?php
        die;
    } else{
        if (!empty($_SESSION['got'])) {
            $_GET = $_SESSION['got'];
            unset($_SESSION['got']);
        }
        if(isset($_GET['passchange'])){
            $messageType = 'error';
            if($_GET['passchange'] == 'fieldsmissing'){
                $message = 'please fill the fields';
            }else if($_GET['passchange'] == 'confirmpass'){
                $message = 'Please confirm your password';
            }else if($_GET['passchange'] == 'samepass'){
                $message = 'The new password is the same as the old one';
            }else if($_GET['passchange'] == 'errorocc'){
                $message = 'An passchange occured ! please try later';
            }else if($_GET['passchange'] == 'wrongpass'){
                $message = 'The password is wrong ! try again';
            }else if($_GET['passchange'] == 'success'){
                $messageType = 'success';
                $message = 'Password Changed successfully';
            }
        }
    }
?>
<div class="uk-container uk-container-medium uk-margin-small uk-flex uk-flex-center middle">
    <form action="user/changePassword" method="post" class="uk-child-width-1-1 uk-width-1-2@s uk-text-center"  data-uk-switcher-item="next">
        <h1 class="uk-text-bold uk-margin-top">Change Your Password</h1>
        <div class="signin-error-messages">
            <div class="signinDangerMessage uk-alert uk-alert-danger uk-hidden"></div>
        </div>
        
        <?php if(isset($_GET['passchange'])) : ?>
            <div class="uk-alert-<?php echo (($messageType == 'error') ? 'danger' : 'success') ?>" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <p><?php echo $message ?></p>
            </div>
        <?php 
            endif;
        ?>
        <div class="uk-inline">
            <span class="uk-form-icon" uk-icon="icon: unlock"></span>
            <input class="uk-input" type="password" name="oldpass" id="oldpass" placeholder="Old Password"> <br>
        </div>
        <br>
        <div class="uk-inline">
            <span class="uk-form-icon" uk-icon="icon: unlock"></span>
            <input class="uk-input" type="password" name="newpass" id="newpass" placeholder="New Password"> <br>
        </div>
        <br>
        <div class="uk-inline">
            <span class="uk-form-icon" uk-icon="icon: unlock"></span>
            <input class="uk-input" type="password" name="newpassconf" id="newpassconf" placeholder="Password Confirmation"> <br>
        </div>
        <input class="uk-button" type="submit" name="changepass" id="changepass" value="Change Password">
    </form>
</div>