<?php 
use ANGIT\Models\UserModel;
    require_once(APP_PATH . DS . 'lib' . DS . 'database.php');
    $emptyUser = new UserModel;
    $questions = $emptyUser->getAllQuestions();
    $countries = $emptyUser->getAllCountries();
    $message = 'There was an error';
    $messageType = 'error';
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    
    if (!empty($_GET)) {
        $_SESSION['got'] = $_GET;
        ?>
        <script>
            window.location.href = "user/join/";
        </script>
        <?php
        die;
    } else{
        if (!empty($_SESSION['got'])) {
            $_GET = $_SESSION['got'];
            unset($_SESSION['got']);
        }
        if(isset($_GET['error'])){
            $messageType = 'error';
            if($_GET['error'] == 'invalidemail'){
                $message = 'Your E-mail is Invalid';
            }else if($_GET['error'] == 'takenemail'){
                $message = 'This E-mail already exists';
            }else if($_GET['error'] == 'inexistantquestion'){
                $message = 'Please Choose a question';
            }else if($_GET['error'] == 'signuperror'){
                $message = 'There was an error ! Please Try Again';
            }else if($_GET['error'] == 'inexistantcountry'){
                $message = 'Please Choose a country';
            }
        }else if(isset($_GET['signup'])){
            if( ($_GET['signup'] == "success") ){
                $messageType = 'success';
                $message = 'Sign Up Successfull';
            }
        }else if(isset($_GET['signin'])){
            $messageType = 'error';
            if($_GET['signin'] == 'invalidsigninemail'){
                $messageType = 'error';
                $message = 'Please enter a valid E-mail Address';
            }else if($_GET['signin'] == 'inexistantemail'){
                $messageType = 'error';
                $message = 'The E-mail address you entered doesn\'t exist';
            }else if($_GET['signin'] == 'wrongpassword'){
                $messageType = 'error';
                $message = 'Wrong Password ! Try Again';
            }else if($_GET['signin'] == 'success'){
                $messageType = 'success';
                $message = 'Sign In Successfull';
                
                ?>
                <script>
                    window.location.href = '/shop/home';
                </script>
                <?php
            }
        }
    }
    $preparedEmail = '';
    if(isset($_POST['footeremailaddress'])){
        $preparedEmail = $_POST['footeremailaddress'];
    }
?>

<?php if(!isset($_SESSION['isSignedIn'])) : ?>
    <ul class="uk-tab uk-child-width-expand" data-uk-tab="{connect:'.registerForm'}">
        <li><a href="">Sign In</a></li>
        <li><a href="">Sign Up</a></li>
    </ul>

<div class="registerForm uk-container uk-container-large uk-text-center uk-switcher">

    <?php if(!isset($_SESSION['isSignedIn'])) : ?>
        <!--Sign In Section-->
        <form action="user/join" method="post" class="uk-child-width-1-1 uk-width-1-2@s uk-text-center"  data-uk-switcher-item="next">
            <div class="signin-error-messages">
                <div class="signinDangerMessage uk-alert uk-alert-danger uk-hidden"></div>
            </div>
            
            <?php if(isset($_GET['signin'])) : ?>
                <div class="uk-alert-<?php echo (($messageType == 'error') ? 'danger' : 'success') ?>" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p><?php echo $message ?></p>
                </div>
            <?php 
                endif;
            ?>
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: mail"></span>
                <input class="uk-input" type="text" name="emailadress" id="emailaddress" placeholder="Email Address" autocomplete="off"> 
            </div>
            <br>
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: unlock"></span>
                <input class="uk-input" type="password" name="passwordsignin" id="passwordsignin" placeholder="Password"> <br>
            </div>
            <input class="uk-button" type="submit" name="signin" id="signin" value="SIGN IN">
        </form>
        <!--Sign In Section-->

        <!--Sign Up Section-->
        <form action="user/join" method="post" class="uk-child-width-1-1 uk-width-1-2@s uk-text-center">
            <div class="signup-error-messages">
                <div class="dangerMessage uk-alert uk-alert-danger uk-hidden"></div>
            </div>

            <?php if(isset($_GET['error']) || isset($_GET['signup'])) : ?>
                <div class="uk-alert-<?php echo (($messageType == 'error') ? 'danger' : 'success') ?>" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p><?php echo $message ?></p>
                </div>
            <?php 
                endif;
            ?>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span> 
                <input class="uk-input" type="text" name="firstname" id="firstname" placeholder="First Name" autocomplete="off"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input class="uk-input" type="text" name="lastname" id="lastname" placeholder="Last Name" autocomplete="off"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: mail"></span>
                <input class="uk-input" type="text" name="emailaddresssignup" id="emailaddresssignup" placeholder="Email Address" value="<?php echo $preparedEmail ?>" autocomplete="off"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: location"></span>
                <input class="uk-input" type="text" name="address" id="streetaddress" placeholder="Street Address" autocomplete="off"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: location"></span>
                <input class="uk-input" type="text" name="address2" id="streetaddress2" placeholder="Street Address 2" autocomplete="off"> <br>
            </div>

            <select class="uk-select" name="countries" id="countries">
                <?php foreach($countries as $country) :?>
                    <option value="<?php echo $country['id'] ?>"><?php echo $country['country_name'] ?></option>
                <?php endforeach;?>
            </select><br>
            
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: world"></span>
                <input class="uk-input" type="text" name="city" id="city" placeholder="City" autocomplete="off"> <br>
            </div>
            
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: info"></span>
                <input class="uk-input" type="text" name="postalCode" id="postalCode" placeholder="Postal Code" autocomplete="off"> <br>
            </div>
            
            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: receiver"></span>
                <input class="uk-input" type="text" name="phonenumber" id="phonenumber" placeholder="Phone Number" autocomplete="off"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: lock"></span>
                <input class="uk-input" type="password" name="passwordsignup" id="passwordsignup" placeholder="Password"> <br>
            </div>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: lock"></span>
                <input class="uk-input" type="password" name="passwordconfsignup" id="passwordconfsignup" placeholder="Password Confirmation"> <br>
            </div>
            

            <select class="uk-select" name="questions" id="questions">
                <?php foreach($questions as $question) :?>
                    <option value="<?php echo $question['ID'] ?>"><?php echo $question['QuestionText'] ?></option>
                <?php endforeach;?>
            </select><br>

            <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: question"></span>
                <input class="uk-input" type="text" name="answer" id="answerInput" placeholder="Answer" autocomplete="off"> <br>
            </div>
            <input class="uk-button" type="submit" name="signup" id='signup' value="SIGN UP">
        </form>
        <!--Sign Up Section-->
    <?php endif; ?>
</div>

<?php else : ?>

<div class="registerForm uk-container uk-container-large uk-text-center">
    <script>
        // window.location.href = '/shop/home';
    </script>
</div>
<?php endif; ?>
