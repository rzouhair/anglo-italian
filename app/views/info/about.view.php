
    
<div class=" main uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light"
    data-src="../images/New_Store_2048x2048.jpg"
    data-srcset="../images/About.jpg" uk-img uk-height-viewport>
    <div class="uk-overlay-primary uk-position-cover"></div>
    <div class="uk-overlay uk-position-bottom-right uk-light">
        <div class="uk-container cont uk-text-left">
            <div class="textSection">
                <h2>ANGLO-ITALIAN COMPANY</h2>
                <p>Anglo-Italian marries the ease of soft Italian <br> construction with the gravity of the English <br> palette. We believe tailoring should be de- <br> constructed, made by hand and made in <br>  Italy. </p>
            </div>
        </div>
    </div>
</div>