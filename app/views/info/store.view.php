
<div class="mainStore uk-height-medium uk-flex uk-flex-center uk-flex-middle uk-background-cover uk-light"
    data-src="../images/New_Store_2048x2048.jpg"
    data-srcset="../images/New_Store_2048x2048.jpg" uk-img uk-height-viewport>
    <div class="uk-overlay-primary uk-position-cover"></div>
    <div class="uk-overlay uk-position-bottom uk-light">
        <div class="uk-container contStore uk-text-left">
            <div class="textSection">
                <h1>OUR STORE</h1>
                <p>57 Weymouth Street, London, W1G 8NP
                    <br>
                    <a href="https://www.google.co.uk/maps/place/57+Weymouth+St,+Marylebone,+London+W1G+8NP/@51.5199418,-0.15297,17z/data=!3m1!4b1!4m5!3m4!1s0x48761ad2251fe6db:0xf9d510d203d496dc!8m2!3d51.5199385!4d-0.1507813" target="_blank" >Get directions with Google maps</a></p>
            </div>
            <div class="textSection">
                <h1>OPENING TIMES</h1>
                <p>Monday – Saturday: 11 – 7
                    <br>Sunday: By appointment</p>
            </div>
            <div class="textSection">
                <h1>CONTACT</h1>
                <p>info@angloitalian.com
                <br>+44(0) 2071 834 492</p>
            </div>
        </div>
    </div>
</div>