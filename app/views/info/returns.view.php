

<div class="uk-container uk-container-xsmall">
    <br>
    <br>
    <br>
    <h2>RETURNS POLICY</h2>
    <p>
        We are happy to offer an exchange or refund on any unsuitable items within 14 days provided they are unworn and in resaleable condition, <br>
        <br>
        Exchanges are only offered for the same style in a different size or colour. If you wish to exchange for a different style, we ask that you request a refund and place a new order for the new item. All refunds exclude all shipping costs.
    </p>

    <h2>HOW TO RETURN YOUR ITEM(S)</h2>
    <p>
        Contact us by email info@angloitalian.com as soon as possible. Include your name, order number, the item you are returning and your preference of a refund or exchange. <br>
        Complete the returns slip that we will provide. Package up your return items securely and post the parcel back to us, using your preferred mail or courier service. Remember to use a recorded service as the goods are your responsibility until they arrive back with us.
    </p>

    <h2>ADDRESS FOR RETURNS</h2>
    <p>
        Anglo-Italian Returns <br>
        57 Weymouth St <br>
        Marylebone <br>
        London <br>
        W1G 8NP <br>
        UK <br>
    </p>


    <h2>FAQS</h2>

    <h2>HOW DO I KNOW IF MY ORDER HAS BEEN SHIPPED?</h2>
    <p>
        We will send you a 'dispatch confirmation' email as soon as your parcel has been collected. This email will also contain your tracking number.
    </p>

    <h2>WILL I BE CHARGED ADDITIONAL TAXES OR DUTY?</h2>
    <p>Customers outside of the EU are responsible for all import duties and tax charges that may be applied by their local customs office on delivery to their country. Any Import duty or tax charges are out of our control.</p>

    <h2>IS MY PARCEL INSURED?</h2>
    <p>We insure all parcels during the time they are in transit until they are delivered to you. We require a signature for any goods delivered, at which point responsibility for the parcel passes to you. If you are not the receiver of your purchase (for example a gift order) then the person who signs for the package confirms receipt and is accountable for the package. Please note, customers are responsible for insuring any return parcels to Anglo Italian.</p>

    <h2>CAN I CHANGE MY DELIVERY ADDRESS?</h2>
    <p>Provided the item has not been collected by our courier, we will do our best to update your preferred address, but we cannot guarantee. Please contact us as soon as possible info@angloitalian.com. Once the item has shipped, we are unable to change the delivery address.</p>

    <h2>CAN I HAVE MY ORDER DELIVERED TO AN ADDRESS DIFFERENT TO MY BILLING ADDRESS?</h2>
    <p>Yes, you can select a separate delivery address from your billing address at the checkout. If you are shopping with us for the first time, we reserve the right to ship your first order to your billing address (rather than an alternative shipping address).</p>

    <h2>WHAT HAPPENS IF NOBODY IS ABLE TO SIGN FOR MY ORDER?</h2>
    <p>UK Mail will leave a card to say they called were unable to deliver and that they will try again the following working day. If the 2nd delivery attempt is unsuccessful, they will leave a card asking you to contact them and rearrange a delivery at a time or address you will be available.</p>

    <h2>DO I HAVE TO PAY FOR MY RETURN?</h2>
    <p>Yes, customers are responsible for the cost of shipping returns back to us. If you have requested an exchange, we will ship the replacement item(s) back to you free of charge.</p>

    <h2>WHEN WILL I RECEIVE MY REFUND?</h2>
    <p>All refunds are processed within 7 days of being returned to our store. We will send you an email to confirm that we have refunded your card. Please note: that refunds can take up to 14 days to credit on your account statement, depending on your card issuer, which is beyond our control.</p>

    <h2>HOW LONG WILL IT TAKE TO SEND OUT MY EXCHANGE ITEMS?</h2>
    <p>Exchanges can take up to 7 days to process. Standard delivery times apply from there on.</p>

    <h2>WILL I BE CHARGED TAX OR DUTY ON MY REPLACEMENT ITEM?</h2>
    <p>
        We mark all replacement shipments as ‘exchange’ or ‘replacement’ with ‘no commercial value’, on the basis taxes would have been paid on the original order. In the unlikely event that your local authorities request taxes on the replacement item, we recommend that you dispute these charges with them directly.
        <br><br>
        If we've not answered your question above, please email info@angloitalian.com 
        Our office hours are 11am – 7pm (GMT) Monday - Friday.
    </p>
    <br>
    <br>
    <br>

    
</div>