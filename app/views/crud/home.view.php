<?php 
    use ANGIT\Models\ShopModel;
    $productInstance = new ShopModel;
    $resultsPerPage = 8;

    if(session_status() == PHP_SESSION_NONE){
        session_start(); 
    }
    
    if(isset($this->params[0])){
        $products = $productInstance->getAllProductsWithLimit($this->params[0], $resultsPerPage);
    }else{
        $products = $productInstance->getAllProductsWithLimit(1, $resultsPerPage);
    }

    if(isset($_SESSION['Update'])){
        if($_SESSION['Update']){
            echo '<div class="uk-alert-success" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record was <strong>Successfully</strong> Updated</p>
                </div>';
            unset($_SESSION['Update']);
        }else{
            echo '<div class="uk-alert-danger" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record Deleting <strong>Failed</strong> </p>
                </div>';
            unset($_SESSION['Update']);
        }
    }

    if(isset($_SESSION['Delete'])){
        if($_SESSION['Delete']){
            echo '<div class="uk-alert-success" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record was <strong>Successfully</strong> Deleted</p>
                </div>';
            unset($_SESSION['Delete']);
        }else{
            echo '<div class="uk-alert-danger" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record Deleting <strong>Failed</strong></p>
                </div>';
            unset($_SESSION['Delete']);
        }
    }

    if(isset($_SESSION['Insert'])){
        if($_SESSION['Insert']){
            echo '<div class="uk-alert-success" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record was <strong>Successfully</strong> Inserted</p>
                </div>';
            unset($_SESSION['Insert']);
        }else{
            echo '<div class="uk-alert-danger" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>The record Inserting <strong>Failed</strong></p>
                </div>';
            unset($_SESSION['Insert']);
        }
    }
?>

<div class="uk-container-xsmall uk-width-1-1 uk-text-center uk-flex uk-flex-center">
    <table class="uk-table uk-table-striped uk-container-small">
        <thead class="uk-text-center">
            <tr>
                <th class='uk-text-center'>Product ID</th>
                <th class='uk-text-center'>Product Name</th>
                <th class='uk-text-center'>Product Price</th>
                <th class='uk-text-center'>Product Category</th>
                <th class='uk-text-center'><a href="#modal-sections" class="uk-icon-button uk-text-muted uk-background-default" uk-icon="plus" uk-toggle></a></th>
            </tr>
        </thead>
        <tbody class="uk-text-center">
            <?php foreach($products as $product) : ?>
                <tr>
                    <td class="Id"><?php echo $product['Id'] ?></td>
                    <td class="Name"><?php echo $product['Name'] ?></td>
                    <td class="Price">£<?php 
                        if(count(explode('.', $product['Price'])) == 1){
                            echo $product['Price'] . '.00';
                        }else{
                            $string = explode('.', $product['Price']);
                            if(strlen(end($string)) == 1){
                                echo $product['Price'] . '0';
                            }else{
                                echo $product['Price'];
                            }
                        }
                    ?> inc.tax</td>
                    <td class="Type"><?php echo ShopModel::getProductTypeFromID($product['Type']) ?></td>
                    <td><a href="/crud/delete/<?php echo $product['Id'] ?>" class="deleteProduct uk-icon-link uk-text-danger" uk-icon="trash"></a> | <a href="#edit-modal-sections" class="uk-icon-link uk-text-primary" uk-icon="pencil" uk-toggle></a></td>
                </tr>
            <?php endforeach;?>
            <tr>
                <td colspan="5" class="pagination">
                    <ul class="uk-width-1-1 uk-flex uk-flex-center uk-flex-middle">
                        <?php 
                            $number_of_pages = $productInstance->PaginationSystem($resultsPerPage); 
                            if($number_of_pages > 1){
                                for($page = 1; $page <= $number_of_pages; $page++){
                                    echo '<li><a href="/crud/home/'. $page .'">'. $page .'</a></li>';
                                }
                            }
                        ?> 
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>

<div id="modal-sections" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Add a new Product</h2>
        </div>
        
        <div class="uk-modal-body">
            <form action="/crud" method="post" enctype="multipart/form-data" >
                <label for="ProductName">Product Name :</label>
                <input class="uk-input" type="text" name="ProductName" id="ProductName"><br>
                <label for="ProductPrice">Product Price</label>
                <input class="uk-input" type="text" name="ProductPrice" id="ProductPrice"><br>
                <label for="ProductDescription">Product Description</label>
                
                <textarea class="uk-textarea" name="ProductDescription" id="ProductDescription" cols="30" rows="5"></textarea><br>

                <label for="ProductType">Product Type :</label>
                <select class="uk-select" name="ProductType" id="ProductType">
                    <?php 
                        foreach (ShopModel::LoadProductTypes() as $type) {
                            echo "<option value='$type'>$type</option>";
                        }
                    ?>
                </select>

                <label for="ProductSizes">Product Size :</label>
                <input class="uk-input" type="text" name="ProductSizes" id="ProductSizes"><br>

                <label for="ProductFile">Product Photos :</label>
                <div class="uk-width-1-1" uk-form-custom="target: true">
                    <input type="file" name="ProductFile[]" id="ProductFile" multiple>
                    <input class="uk-input uk-form-width-medium uk-width-1-1" type="text" placeholder="Select files" disabled>
                </div>
                <hr>
                 <input style="background-color: #1c1e3b" id="save" class="uk-float-right uk-button uk-button-primary" type="submit" name="submit" value="Submit" />
                 
                <button class="uk-float-right uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                
            </form>
        </div>
    </div>
</div>

<div id="edit-modal-sections" uk-modal>
    <div class="uk-modal-dialog">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Add a new Product</h2>
        </div>
        
        <div class="uk-modal-body">
            <form action="/crud/edit" method="post" enctype="multipart/form-data" >
                <label for="EditProductId" >Product ID :</label>
                <input style="background-color: #F7F7F7" class="uk-input" type="text" name="EditProductId" id="EditProductId" readonly ><br>
                <label for="EditProductName" >Product Name :</label>
                <input class="uk-input" type="text" name="EditProductName" id="EditProductName"><br>
                <label for="ProductPrice">Product Price</label>
                <input class="uk-input" type="text" name="EditProductPrice" id="EditProductPrice"><br>
                <label for="EditProductDescription">Product Description</label>
                
                <textarea class="uk-textarea" name="EditProductDescription" id="EditProductDescription" cols="30" rows="5"></textarea><br>

                <label for="EditProductType">Product Type :</label>
                <select class="uk-select" name="EditProductType" id="EditProductType">
                    <?php 
                        foreach (ShopModel::LoadProductTypes() as $type) {
                            echo "<option value='$type'>$type</option>";
                        }
                    ?>
                </select>

                <label for="EditProductSizes">Product Size :</label>
                <input class="uk-input" type="text" name="EditProductSizes" id="EditProductSizes"><br>

                <label for="EditProductFile">Product Photos :</label>
                <div class="uk-width-1-1" uk-form-custom="target: true">
                    <input type="file" name="EditProductFile[]" id="EditProductFile" multiple>
                    <input class="uk-input uk-form-width-medium uk-width-1-1" type="text" placeholder="Select files" disabled>
                </div>
                <hr>
                 <input style="background-color: #1c1e3b" id="edit" class="uk-float-right uk-button uk-button-primary" type="submit" name="edit" value="Edit" />
                 
                <button class="uk-float-right uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
            </form>
        </div>
    </div>
</div>

<script>
    
</script>