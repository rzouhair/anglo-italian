<?php 
    use ANGIT\Models\ShopModel;
use ANGIT\Models\UserModel;
    $UserID = 1;
    $ProductsInBag = array();
    $userInstance = new UserModel;
    $shopInstance = new ShopModel;
    if(session_status() == PHP_SESSION_NONE){
        session_start();
    }
    if(isset($_SESSION['SignedInUserID'])){
        $UserID = $_SESSION['SignedInUserID'];
        $ProductsInBag = $userInstance->getAllProductsInBag($UserID);
    }

?>
<div class="uk-container uk-container-xsmall uk-text-center">
        <?php if(!empty($ProductsInBag)) :?>
            <h1 class="uk-h1 bagTitle">YOUR BAG</h1>
            
            <section class="bagProducts">
                <?php if(isset($ProductsInBag)) :
                    foreach($ProductsInBag as $key => $product) : 
                        $thisProducts = $shopInstance->getProductByID($product['ProductID'])[0];
                        $image = $shopInstance->getPhotos($thisProducts['Id']);
                        ?>
                        
                        <div class="prod">
                            <img src="../images/<?php echo $image[0] ?>" alt="prod">
                            <p id="bagProductName"><?php echo $thisProducts['Name'] ?></p>
                            <p id="bagProductSize"><?php echo $product['ProductSize'] ?></p>
                            <p id="quantityManager">
                                <span id="minus">-</span>
                                &nbsp;&nbsp;&nbsp; <span class="quantity"><?php echo $product['ProductQuantity'] ?></span> &nbsp;&nbsp;&nbsp;
                                <span id="plus">+</span>
                            </p>
                            <p id="Price">£ <span id="priceText"><?php echo $shopInstance->prettifyPrice($thisProducts['Price'] * $product['ProductQuantity']) ?></span> <small>INC. TAX</small> </p>
                            <p id="trashIcon"><span uk-icon="icon: trash"></span></p>
                        </div>
                <?php
                        endforeach;
                        ?>
                        <!-- Total Section -->
                        <section class="bagProductsTotal">
                            <div class="prodTotal">
                                <h3>Total</h3>
                                <h3 class="uk-margin-remove uk-padding-remove uk-text-success uk-text-bold">£ <span id="totalPriceText"><?php echo($shopInstance->prettifyPrice($userInstance->getTotalCartPrice($UserID))) ?></span> <small> <small>INC. TAX</small> </small> </h3>
                            </div>
                        </section>
                        <!-- ./Total Section -->

                        <section class="bagButtons uk-visible@s uk-text-center">
                            <form action="#" method="post">
                                <input type="submit" value="CONTINUE SHOPPING">
                                <input type="submit" value="CHECK OUT">
                                <input type="submit" value="">
                            </form>
                        </section>
                        <section class="uk-visible uk-hidden@s bagButtons2 uk-width-1-1 uk-text-center">
                            <form action="#" method="post" class="">
                                <input class="uk-width-1-1" type="submit" value="CONTINUE SHOPPING">
                                <input class="uk-width-1-1" type="submit" value="CHECK OUT">
                                <input class="uk-width-1-1" type="submit" value="">
                            </form>
                        </section>
                        <?php
                    endif;
                ?>
            </section>

            <?php if(isset($products)) :?>
            
            
            <?php endif; ?>
        <?php elseif (empty($ProductsInBag) || !isset($_SESSION['isSignedIn'])) :
                if(!isset($_SESSION['isSignedIn'])){
                    echo '<h1 class="uk-h1 bagTitle">Please Sign In/Up to Continue</h1>';
                }else{
                    echo '<h1 class="uk-h1 bagTitle">YOUR BAG IS EMPTY</h1>';
                }
        endif; ?>

</div>