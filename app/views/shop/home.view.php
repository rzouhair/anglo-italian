<?php 
    use ANGIT\Models\ShopModel;
    $types = ShopModel::getAllProductTypes();
    $productInstance = new ShopModel;
    $resultsPerPage = 24;
    $currentCategory = 'all';
    $currentPage = 1;
    $allCategories = $productInstance->getAllCategories();

    if(isset($this->params[0]) && $this->params[0] != 'all'){
        if(in_array($this->params[0], $types) || $this->params[0] == 'all'){
            if($this->params[0] != 'all'){
                if(isset($this->params[1])){
                    $all_products = ShopModel::getProductsByCategoryWithLimit($this->params[0], $this->params[1], $resultsPerPage);
                }else{
                    $all_products = ShopModel::getProductsByCategoryWithLimit($this->params[0], 1, $resultsPerPage);
                }
            }else{
                $all_products = $productInstance->getAllProductsWithLimit(1, $resultsPerPage);
            }
        }else{
            
            ?>
            <script>
                window.location.href = "/notfoundpage";
            </script>
            <?php
        }
        $currentCategory = $this->params[0];
    }else{
        if(isset($this->params[1])){
            $all_products = $productInstance->getAllProductsWithLimit($this->params[1], $resultsPerPage);
        }else{
            $all_products = $productInstance->getAllProductsWithLimit(1, $resultsPerPage);
        }
    }

    if(isset($this->params[1])){
        $currentPage = $this->params[1];
    }
    
    $productInstance->getAllProducts();
?>

<div class="uk-container uk-container-large" uk-filter="target: .js-filter">
    <!--.Categories-->
    <section class="categories uk-flex uk-flex-center uk-flex-middle uk-width-1-1">
        <ul class=" uk-flex uk-flex-center uk-flex-middle uk-grid-xsmall uk-margin-large-top uk-margin-medium-bottom uk-margin-xsmall-left" uk-grid>
            <li class="categ uk-width-small@s uk-width-1-4"><a id="<?php echo ($currentCategory == 'all') ? 'currentCategory' :'all'  ?>" href="#">all</a></li>
            <?php foreach($allCategories as $category) : ?>

            <li class="categ uk-width-small@s uk-width-1-4"><a id="<?php echo ($currentCategory == strtolower($category['Label'])) ? 'currentCategory' : $category['Label']  ?>" href="#"><?php echo $category['Label'] ?></a></li>
            
            <?php endforeach ?>
        </ul>
    </section>
    <!--/.Categories-->

    <!--.Products-->
    <div class="products uk-container uk-width-1-1 uk-child-width-1-4@l uk-child-width-1-2 uk-flex-center" uk-grid>
        <div class='uk-width-1-1' >
            <ul class="product uk-child-width-1-2 uk-child-width-1-4@l uk-text-center" uk-grid>
            <?php foreach($all_products as $product) : ?>
                <li>
                    <section class="product uk-flex uk-flex-center uk-flex-middle uk-text-right test1">
                        <div  class="uk-width-1-1 uk-text-center">
                            <img src="images/<?php echo explode('|', $product['Photos'])[0] ?>" alt="Photo">
                        </div>
                        <div class="color-overlay">
                            <h5> <span id="ProdName"> <?php echo $product['Name'] ?> </span> <br><br>£<?php echo $productInstance->prettifyPrice($product['Price']) ?> <small>inc.tax</small></h5>

                            <div class="sizes">
                                <?php foreach($productInstance->getProductSizes($product['Id']) as $size) : ?>
                                    <label class="<?php echo ($size['Quantity'] > 0) ? 'available' : 'unavailable' ?>"> <?php echo $size['Size'] ?> &nbsp; </label>
                                <?php endforeach; ?>
                            </div>

                            <?php if($productInstance->getProductSum($product['Id']) > 0 ) : ?>
                                <p>In Stock</p>
                            <?php else : ?>
                                <p>Out of Stock</p>
                            <?php endif; ?>
                        </div>
                    </section>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
        
    </div>
    <!--/.Products-->

    <!--Pagination-->
    <section class="pagination uk-width-1-1 uk-flex uk-flex-center uk-flex-middle">
        <ul>
            <?php 
            
                if(isset($this->params[0]) && $this->params[0] != 'all'){
                    $number_of_pages = $productInstance->PaginationSystemByCategory($this->params[0] ,$resultsPerPage); 
                }else{
                    $number_of_pages = $productInstance->PaginationSystem($resultsPerPage); 
                }

                if($currentPage > $number_of_pages){
                    ?>
                    <script>
                        window.location.href = "/notfoundpage";
                    </script>
                    <?php
                }
                if($number_of_pages > 1){
                    for($page = 1; $page <= $number_of_pages; $page++){
                        if(isset($this->params[1])){
                            if($page == $this->params[1]){
                                if(isset($this->params[0])){
                                    echo '<li><a id="currentPage" href="/shop/home/'. $this->params[0] .'/'. $page .'">'. $page .'</a></li>';
                                }else{
                                    echo '<li><a id="currentPage" href="/shop/home/all/'. $page .'">'. $page .'</a></li>';
                                }
                            }else{
                                
                                if(isset($this->params[0])){
                                    echo '<li><a href="/shop/home/'. $this->params[0] .'/'. $page .'">'. $page .'</a></li>';
                                }else{
                                    echo '<li><a href="/shop/home/all/'. $page .'">'. $page .'</a></li>';
                                }
                            }
                        }else{
                            if($page != 1){
                                if(isset($this->params[0])){
                                    echo '<li><a href="/shop/home/'. $this->params[0] .'/'. $page .'">'. $page .'</a></li>';
                                }else{
                                    echo '<li><a href="/shop/home/all/'. $page .'">'. $page .'</a></li>';
                                }
                            }else{
                                if(isset($this->params[0])){
                                    echo '<li><a id="currentPage" href="/shop/home/'. $this->params[0] .'/'. $page .'">'. $page .'</a></li>';
                                }else{
                                    echo '<li><a id="currentPage" href="/shop/home/all/'. $page .'">'. $page .'</a></li>';
                                }
                            }
                        }
                    }
                }
            ?> 
        </ul>
    </section>
    <!--/Pagination-->
</div>
