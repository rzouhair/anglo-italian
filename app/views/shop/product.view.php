<?php 
    use ANGIT\Models\ShopModel;
    $emptyProduct = new ShopModel;
    $fetchProduct = array();
    if(isset($this->params[0])){
        $name = $this->params[0];
        $name = str_replace('-', ' ', $name);
        $name = str_replace('_', '-', $name);
        $name = str_replace('!', '"', $name);
        if($emptyProduct->checkProductName($name)){
            $fetchProduct = $emptyProduct->getProductByName($name)[0];
            $alreadyExistsInBag = $emptyProduct->checkProductInBag($fetchProduct['Id']);
        }else{
            ?>
                <script type="text/javascript">
                    window.location.href = '/notfoundproduct';
                </script>
            <?php
            exit();
        }
    }else{
        ?>
            <script type="text/javascript">
                window.location.href = '/notfoundproduct';
            </script>
        <?php
        exit();
    }
    
?>

<div class="uk-container uk-container-large">
    <div class="productInfos uk-width-1-1 uk-child-width-1-2@m uk-child-width-1-1 uk-flex-center uk-flex-middle" uk-grid>
        <!-- Image Section -->
        <section class="productImage">
            <div class="slider1 uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">
                <ul class="uk-slider-items uk-grid">
                <?php foreach(explode('|', $fetchProduct['Photos']) as $photo) :?>
                    <li class="uk-width-3-4">
                        <div class="zoom">
                            <div class="uk-panel">
                                <img class="productImages uk-width-large uk-padding uk-padding-remove-bottom" src="../images/<?php echo $photo ?>" alt="">
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
                </ul>
                <a class="slidenav uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="slidenav uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
            
            </div>
            <h6>Click to maximize</h6>
        </section>
        <!-- ./Image Section -->

        <!-- Product Infos Section -->
        <section class="productPrice uk-margin-large uk-text-left">
            <!-- Name Section -->
            <h2 id="ProdName"><?php echo $fetchProduct['Name'] ?></h2>
            <h2 class='uk-margin uk-margin-medium-top uk-margin-medium-bottom'>£<?php echo $emptyProduct->prettifyPrice($fetchProduct['Price']) ?> <span><small>inc.tax</small></span> </h2>
            <!-- Name Section -->


            <!-- Sizes Section -->
            <?php if(!in_array($fetchProduct['Type'], [6, 7])) :?>
                <h4>Size :</h4>
            <?php endif;?>

            <ul class="sizes">
                <?php foreach($emptyProduct->getProductSizes($fetchProduct['Id']) as $size) :?>
                    <li><a class="size <?php echo ($size['Quantity'] == 0) ? 'unavailable' : '' ?>" href="#"><?php echo $size['Size'] ?></a></li>
                <?php endforeach; ?>
            </ul>
            <!-- ./Sizes Section -->

            <!-- Buttons Section -->
            <form action="#" method="post">
                <?php if(isset($alreadyExistsInBag) && !$alreadyExistsInBag ) : ?>
                
                    <?php if($fetchProduct['Quantity'] > 0 ) : ?>
                        <input type="submit" id="addtobag" name="addtobag" value="ADD TO BAG">
                    <?php else : ?>
                        <input type="submit" id="soldout" name="soldout" value="SOLD OUT" disabled>
                    <?php endif; ?>

                <?php elseif(isset($alreadyExistsInBag) && $alreadyExistsInBag ) : ?>

                    <input type="submit" id="addtobag" name="addtobag" value="ADDED TO YOUR BAG" disabled>
                    
                <?php else: ?>

                    <?php if($emptyProduct->getProductSum($fetchProduct['Id']) > 0 ) : ?>
                        <input type="submit" id="addtobag" name="addtobag" value="ADD TO BAG <?php echo (isset($_SESSION['isSignedIn']) ? '' : '(You\'re not signed in)') ?>" <?php echo (isset($_SESSION['isSignedIn']) ? '' : 'disabled') ?>>
                    <?php else : ?>
                        <input type="submit" id="soldout" name="soldout" value="SOLD OUT" disabled>
                    <?php endif; ?>

                <?php endif; ?>
            </form>
            <!-- ./Buttons Section -->

            <p><?php echo $fetchProduct['Description'] ?></p>
            
            <!-- PS Section -->
            <?php if($fetchProduct['Type'] == 2) : ?>
                <p style="color: black; font-weight: 700; font-size: 1.5em"><span style="color: red;">NOTE </span>' = ½</p>
            <?php endif; ?>
            <!-- PS Section -->

        </section>
        <!-- ./Product Infos Section -->
    </div>

    <!-- Maximized Image Section -->
    <div class="productLightbox uk-hidden uk-child-width-1-3@m" uk-grid uk-lightbox="animation: slide">
        <?php foreach(explode('|', $fetchProduct['Photos']) as $index => $photo) :?>
            <div>
                <a class="uk-inline" href="../images/<?php echo $photo ?>" data-caption="<?php echo $index+1 . '/' . count(explode('|', $fetchProduct['Photos'])) ?>">
                    <img src="../images/<?php echo $photo ?>" alt="">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- Maximized Image Section -->
    
    <?php if(!in_array($fetchProduct['Type'], [6, 7])) :?>
        <div class="circle uk-hidden"></div>
    <?php endif;?>

</div>


