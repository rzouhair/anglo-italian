<?php 
namespace ANGIT\Controllers;

use ANGIT\Models\UserModel;


class UserController extends AbstractController{
    public function joinAction(){
        //Sign Up Method
        if(isset($_POST['signup'])){
            $firstname = trim($_POST['firstname'], ' ');
            $lastname = trim($_POST['lastname'], ' ');
            $email = trim($_POST['emailaddresssignup'], ' ');
            $address = trim($_POST['address'], ' ');
            $address2 = trim($_POST['address2'], ' ');
            $countries = $_POST['countries'];
            $city = $_POST['city'];
            $phoneNumber = $_POST['phonenumber'];
            $postalCode = $_POST['postalCode'];
            $password = $_POST['passwordsignup'];
            $passwordConfirmation = $_POST['passwordconfsignup'];
            $question = $_POST['questions'];
            $answer = $_POST['answer'];
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

            $newUser = UserModel::constructor($firstname, $lastname, $email, $address, $address2, $countries, $city, $phoneNumber, $postalCode, $hashedPassword, $question, $answer);

            if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){
                header('Location: /user/join?error=invalidemail');
                exit();
            }

            if($password !== $passwordConfirmation){
                header('Location: /user/join?error=unidenticalpasswords');
                exit();
            }

            if($newUser->checkEmailExistance($email)){
                header('Location: /user/join?error=takenemail');
                exit();
            }
            
            if(!$newUser->checkQuestionExistance($question)){
                header('Location: /user/join?error=inexistantquestion');
                exit();
            }
            
            if(!$newUser->checkCountryExistance($countries)){
                header('Location: /user/join?error=inexistantcountry');
                exit();
            }

            
            if(!$newUser->InsertUser()){
                header('Location: /user/join?error=signuperror');
                exit();
            }else{
                header('Location: /user/join?signup=success');
                exit();
            }
            

        }

        //Sign In Method
        if(isset($_POST['signin'])){
            $emptyUser = new UserModel;
            $email = trim($_POST['emailadress'], ' ');
            $password = $_POST['passwordsignin'];

            if(filter_var($email, FILTER_VALIDATE_EMAIL) == false){
                header('Location: /user/join?signin=invalidsigninemail');
                exit();
            }

            if(!$emptyUser->checkEmailExistance($email)){
                header('Location: /user/join?signin=inexistantemail');
                exit();
            }
            
            if(!$emptyUser->CheckPassword($email, $password)){
                header('Location: /user/join?signin=wrongpassword');
                exit();
            }else{
                session_start();
                $_SESSION['isSignedIn'] = true;
                $signedInUser = $emptyUser->GetUserInfos($email)[0];
                $_SESSION['SignedInUserID'] = $signedInUser['ID'];
                header('Location: /user/join?signin=success');
                exit();
            }
            

        }

        //Log Out Method
        if(isset($_POST['logout'])){
            session_start();
            session_unset();
            session_destroy();
        }

        $this->view();
        
    }

    public function changePasswordAction(){
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }
        if(isset($_POST['changepass'])){
            $oldPassword = $_POST['oldpass'];
            $newPassword = $_POST['newpass'];
            $newPasswordConfirmation = $_POST['newpassconf'];

            $emptyUser = new UserModel;
            $changepassResult = $emptyUser->ChangePassword($_SESSION['SignedInUserID'], $oldPassword, $newPassword, $newPasswordConfirmation);
            if($changepassResult !== true){
                header('Location: /user/changePassword?passchange='. $changepassResult);
            }else{
                header('Location: /user/changePassword?passchange=success');
            }
        }
        $this->view();
    }
}
?>