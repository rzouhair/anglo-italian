<?php 
namespace ANGIT\Controllers;

use ANGIT\Models\ShopModel;

class ShopController extends AbstractController{
    public function homeAction(){
        $this->view();
    }

    public function productAction(){
        $this->view();
    }

    public function bagAction(){
        $this->view();
    }
}
?>