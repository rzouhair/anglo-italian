<?php 
namespace ANGIT\Controllers;
use ANGIT\Models\ShopModel;

class CrudController extends AbstractController{
    public function homeAction(){
        if(isset($_POST['submit'])){
            $productName = $_POST['ProductName'];
            $productPrice = $_POST['ProductPrice'];
            $productSize = $_POST['ProductSizes'];
            $productType = $_POST['ProductType'];
            $productDescription = $_POST['ProductDescription'];
            $productPhotos = ShopModel::uploadFile('ProductFile', UPLOAD_DESTINATION_PATH);

            $Product = ShopModel::constructor($productName, $productPrice, $productSize, $productType, $productDescription, $productPhotos);

            if($Product->InsertProduct()){
                $_SESSION['Insert'] = true;
            }else{
                $_SESSION['Insert'] = false;
                echo $Product->InsertProduct();
            }

        }
        $this->view();
    }

    public function deleteAction(){
        $id = $this->params[0];
        $product = new ShopModel;
        
        $product->deleteProductPhotos($id);
        if($product->deleteProduct($id)){
            $_SESSION['Delete'] = true;
        }else{
            $_SESSION['Delete'] = false;
        }
        header("Location: /crud/");
        exit();
    }

    public function editAction(){
        if(isset($_POST['edit'])){
            $productId = $_POST['EditProductId'];
            $productName = $_POST['EditProductName'];
            $productPrice = $_POST['EditProductPrice'];
            $productSize = $_POST['EditProductSizes'];
            $productType = $_POST['EditProductType'];
            $productDescription = $_POST['EditProductDescription'];
            $productPhotos = ShopModel::uploadFile('EditProductFile', UPLOAD_DESTINATION_PATH);

            $Product = ShopModel::constructor($productName, $productPrice, $productSize, $productType, $productDescription, $productPhotos);

            
            if(($_FILES['EditProductFile']['name'][0]) == ""){
            }else{
                $Product->deleteProductPhotos($productId);
            }
            
            if($Product->editProduct($productId)){
                echo 'Successfully Updated <br>Product ID :' . $productId;
                $_SESSION['Update'] = true;
            }else{
                echo 'Failed Updating';
                $_SESSION['Update'] = false;
            }

            header('Location: /crud');
            exit();

        }
    }
}

?>
