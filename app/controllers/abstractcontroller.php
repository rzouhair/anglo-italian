<?php 
namespace ANGIT\Controllers;
class AbstractController{

    protected $controller;
    protected $action;
    protected $params;

    public function notFoundAction(){
        require_once(TEMPLATES_PATH . DS . 'navbar.php');
        $this->view();
        require_once(TEMPLATES_PATH . DS . 'footer.php');
    }

    public function setController($controller){
        $this->controller = $controller;
    }

    public function setAction($action){
        $this->action = $action;
    }

    public function setParams($params){
        $this->params = $params;
    }

    public function view(){
        $view = VIEWS_PATH .DS. $this->controller .DS. $this->action . '.view.php';
        if($this->controller == NOT_FOUND_CONTROLLER || $this->action == NOT_FOUND_ACTION){
            $view = VIEWS_PATH .DS. 'notfound' .DS. 'notfound.view.php';
        }
        
        require_once(TEMPLATES_PATH . DS . 'navbar.php');
        require_once($view);
        require_once(TEMPLATES_PATH . DS . 'footer.php');
    }
}
?>