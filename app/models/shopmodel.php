<?php 
namespace ANGIT\Models;

class ShopModel extends AbstractModel{
    private $ProductName;
    private $ProductPrice;
    private $ProductSizes;
    private $ProductType;
    private $ProductDescription;
    private $ProductPhotos;

    
    //No Arguments constructor
    public function __construct()
    {
    }

    //Primary Init Constructor
    public static function constructor($ProductName, $ProductPrice, $ProductSizes, $ProductType, $ProductDescription, $ProductPhotos){
        $instance = new self();
        $instance->ProductName = $ProductName;
        $instance->ProductPrice = $ProductPrice;
        $instance->ProductSizes = $ProductSizes;
        $instance->ProductType = $ProductType;
        $instance->ProductDescription = $ProductDescription;
        $instance->ProductPhotos = $ProductPhotos;
        return $instance;
    } 

    //Get a good price look i.e 135 --> 135.00 | 1200.98 --> 1,200.98
    public function prettifyPrice($price){
        $priceParts = explode('.', $price);
        $firstPart = $priceParts[0];
        if(strlen($firstPart) > 3){
            $firstPart = substr($firstPart, -strlen($firstPart), -3) . ',' .substr($firstPart, -3);
        }
        if(count($priceParts) > 1){
            $secondPart = $priceParts[1];
            if(strlen($secondPart) > 1){
                return $firstPart . '.' . $secondPart;
            }else{
                return $firstPart . '.' . $secondPart . '0'; 
            }
        }else{
            return $firstPart . '.00';
        }
    }

    //Append a JSON File from the database
    public function AppendJSON($JSON_file_path = 'webservice/Products.json', $data){
        if(file_exists($JSON_file_path)){
            if(file_put_contents($JSON_file_path, $data)){
                
            }else{
                echo 'File Append Failed';
            }
        }else{
            echo 'Your File Doesn\'t exist';
        }
    }

    //Creating a pagination system by category
    public function PaginationSystemByCategory($category, $resultsPerPage){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        //Result Per Page
        
        $select_all_rows = "SELECT * FROM product WHERE Type = " . self::getProductTypeFromLabel($category);
        $number_of_records_result = mysqli_query($connection, $select_all_rows);
        //Number Of Records on the desired Table
        $number_of_records = mysqli_num_rows($number_of_records_result);

        $number_of_page = ceil($number_of_records / $resultsPerPage);

        return $number_of_page;
    }

    //Creating a pagination system
    public function PaginationSystem($resultsPerPage){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        //Result Per Page
        
        $select_all_rows = "SELECT * FROM product";
        $number_of_records_result = mysqli_query($connection, $select_all_rows);
        //Number Of Records on the desired Table
        $number_of_records = mysqli_num_rows($number_of_records_result);

        $number_of_page = ceil($number_of_records / $resultsPerPage);

        return $number_of_page;
    }

    //Getting all Products from our database with limit for pagination
    public function getAllProductsWithLimit($param = 1, $resultsPerPage){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        if(!isset($param)){
            $current_page = 0;
        }else{
            $current_page = ($param - 1) * $resultsPerPage;
        }

        $select_all_products = "SELECT * FROM Product GROUP BY Id ASC LIMIT $current_page, $resultsPerPage";
        $all_products_result = mysqli_query($connection, $select_all_products);

        $all_products = array();

        while($product = mysqli_fetch_assoc($all_products_result)){
            $all_products[] = $product;
        }

        return $all_products;
    }

    //Getting all Categories from our database
    public static function getAllCategories(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $select_all_categories = "SELECT Label FROM producttype GROUP BY Id ASC";
        $all_categories_result = mysqli_query($connection, $select_all_categories);

        $all_categories = array();

        while($product = mysqli_fetch_assoc($all_categories_result)){
            $all_categories[] = $product;
        }

        return $all_categories;
    }

    //Getting all Products from our database
    public static function getAllProducts(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $select_all_products = "SELECT * FROM Product GROUP BY Id ASC";
        $all_products_result = mysqli_query($connection, $select_all_products);

        $all_products = array();

        while($product = mysqli_fetch_assoc($all_products_result)){
            $all_products[] = $product;
        }
        $product_to_parse = json_encode($all_products);
        $emptyInstance = new ShopModel;
        $emptyInstance->AppendJSON('webservice/Products.json', $product_to_parse);

        return $all_products;
    }

    //Getting all Products from our database
    public static function searchProducts($keyword){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $keywordParam = '%'. $keyword .'%';
        $search_all_products = "SELECT * FROM product WHERE Name LIKE ? GROUP BY Id LIMIT 6";
        
        $statement = mysqli_stmt_init($connection);

        $all_products = array();
        if(mysqli_stmt_prepare($statement, $search_all_products)){
            mysqli_stmt_bind_param($statement, 's', $keywordParam);
            mysqli_stmt_execute($statement);
            $result = mysqli_stmt_get_result($statement);
            while($searchResult = mysqli_fetch_assoc($result)){
                $all_products[] = $searchResult;
            }
        }
        return $all_products;
    }
    
    //Getting the product By passing the ID as an argument
    public function getProductByID($id){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT * FROM product WHERE Id = ?";
        $product = array();
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            mysqli_stmt_bind_param($statment, "i", $id);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                $product[] = $row;
            }
        }
        return $product;
    }
    
    //Getting the product By passing the ID as an argument
    public function getProductByName($name){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT * FROM product WHERE Name = ?";
        $product = array();
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            mysqli_stmt_bind_param($statment, "s", $name);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                $product[] = $row;
            }
        }
        return $product;
    }

    //Getting the products according to the passed category 
    public static function getProductsByCategory($category){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT * FROM product WHERE Type = ?";
        $product = array();
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            $CategoryID = self::getProductTypeFromLabel($category);
            mysqli_stmt_bind_param($statment, "i", $CategoryID);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                $product[] = $row;
            }
        }
        return $product;
    }

    //Getting the products according to the passed category 
    public static function getProductsByCategoryWithLimit($category, $param, $resultsPerPage){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        if(!isset($param)){
            $current_page = 0;
        }else{
            $current_page = ($param - 1) * $resultsPerPage;
        }

        $CategoryID = self::getProductTypeFromLabel($category);

        $query = "SELECT * FROM product WHERE Type = $CategoryID GROUP BY Id LIMIT $current_page, $resultsPerPage";

        $all_products = array();

        $product_by_category_limit_result = mysqli_query($connection, $query);

        while($product = mysqli_fetch_assoc($product_by_category_limit_result)){
            $all_products[] = $product;
        }

        return $all_products;
    }

    //Ckeck whether the ID exists in the database or not
    public function checkProductID($id){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT Id FROM Product";
        $product = array();
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                // $product[] = $row['Id'];
                $product[] = $row['Id'];
            }
        }
        return in_array($id, $product);
    }

    //Ckeck whether the ID exists in the database or not
    public function checkProductName($name){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT Name FROM Product";
        $product = array();
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                $product[] = strtolower($row['Name']);
            }
        }
        return in_array($name, $product);
    }

    //Getting all product Sizes as an array
    public static function ParseSizes($sizes){
        $sizesArray = explode(' ', $sizes);
        return $sizesArray;
    }

    //Loading all product Types
    public static function LoadProductTypes(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $types = array();
        $select_all_product_types = "SELECT Label FROM ProductType";
        $product_types_result = mysqli_query($connection, $select_all_product_types);
        while($row = mysqli_fetch_assoc($product_types_result)){
            $types[] = $row['Label'];
        }
        return $types;
    }
    
    //Getting the product type ID from the label
    public static function getProductTypeID($productType){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_product_type_id = "SELECT Id FROM producttype WHERE Label = '$productType'";
        $id = mysqli_query($connection, $select_product_type_id)->fetch_object()->Id;
        return $id;
    }

    //Getting the product type label from the ID (Primary key)
    public static function getProductTypeFromID($productTypeID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_product_type_id = "SELECT Label FROM producttype WHERE Id = '$productTypeID'";
        $Label = mysqli_query($connection, $select_product_type_id)->fetch_object()->Label;
        return $Label;
    }

    //Getting the product type label from the ID (Primary key)
    public static function getProductTypeFromLabel($productTypeLabel){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_product_type_id = "SELECT Id FROM producttype WHERE Label = ?";
        $CategoryId = "";
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $select_product_type_id)){
            mysqli_stmt_bind_param($statment, 's', $productTypeLabel);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while($Ids = mysqli_fetch_assoc($result)){
                $CategoryId = $Ids['Id'];
            }
        }
        return $CategoryId;
    }

    //Getting all product types
    public static function getAllProductTypes(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $select_all_products = "SELECT Label FROM producttype";
        $all_products_result = mysqli_query($connection, $select_all_products);

        $all_products_types = array();

        while($type = mysqli_fetch_assoc($all_products_result)){
            $all_products_types[] = strtolower($type['Label']);
        }

        return $all_products_types;
    }
    
    // TODO :: Complete the Delete Product Method using the ID by deleting all it's photos
    public function deleteProduct($id){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $delete_product_query = "DELETE FROM `product` WHERE (Id = $id)";

        $status = false;
        if(mysqli_query($connection ,$delete_product_query) === TRUE){
            $status = true;
        }else{
            $status = false;
        }

        return $status;
    } 

    // TODO :: Complete the Edit Product Method by deleting all it's photos
    public function editProduct($Id){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $update_product = "UPDATE product SET Name = ? , Price = ? , Sizes = ? , Type = ? , Description = ? , Photos = ? WHERE Id = ?";

        $type = self::getProductTypeID($this->ProductType);
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $update_product)){
            mysqli_stmt_bind_param($statment, 'sdsissi', $this->ProductName, $this->ProductPrice, $this->ProductSizes, $type, $this->ProductDescription, $this->ProductPhotos, $Id);
            if(mysqli_stmt_execute($statment)){
                echo 'SUCCESS';
                return true;
            }else{
                echo 'FAILED';
                return false;
            }
        }
    }

    //Inserting a product
    public function InsertProduct(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $insert_product_query = "INSERT INTO  product (Name , Price , Sizes , Type , Description , Photos) VALUES (?, ?, ?, ?, ?, ?)";
        $statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statement, $insert_product_query)){
            $productType = $this->getProductTypeID($this->ProductType);

            mysqli_stmt_bind_param($statement, 'sdsiss', $this->ProductName, $this->ProductPrice, $this->ProductSizes, $productType, $this->ProductDescription, $this->ProductPhotos);
            mysqli_stmt_execute($statement);
            return true;
        }else{
            return false;
        }
        
    }

    //Getting all Photos for a specific product by passing it's ID as an argument
    public function getPhotos($id){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $query = "SELECT Photos FROM Product WHERE Id = ?";
        $photos = "";
        $statment = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($statment, $query)){
            mysqli_stmt_bind_param($statment, 'i', $id);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            while ($row = mysqli_fetch_assoc($result)) {
                $photos = $row['Photos'];
                
            }
            return $this->ParsePhotosRecord($photos);
        }else{
            return 'ERROR';
        }
    }

    //Delete Product Specific ID
    public function deleteProductPhotos($id){
        $allProductPhotos = $this->getPhotos($id);
        foreach ($allProductPhotos as $photo) {
            unlink(UPLOAD_DESTINATION_PATH . DS . $photo);
        }
        
    }

    //Getting al photos Names as an array 
    public function ParsePhotosRecord($photos){
        if($photos !== ""){
            $photosArray = array();
            $photos = explode("|", $photos);
            foreach ($photos as $photo) {
                $photosArray[] = $photo;
            }
            return $photosArray;
        }else{
            return '';
        }
    }

    //Getting the Product ID by passing the name as an argument (The Name is unique in the database)
    public function getProductIdByName($Name){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_product_id = "SELECT Id FROM Product WHERE Name = ?";
        $statment = mysqli_stmt_init($connection);

        $id = "";

        if(mysqli_stmt_prepare($statment, $get_product_id)){
            mysqli_stmt_bind_param($statment, 's', $Name);
            mysqli_stmt_execute($statment);
            $result = mysqli_stmt_get_result($statment);
            $id = $result->fetch_object()->Id;
        }

        return $id;
    }
    
    //Checking whether the ID of the product exists in the bag or not
    public function checkProductInBag($id){
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }

        if(isset($_SESSION['Bag'])){
            foreach($_SESSION['Bag'] as $prod){
                if($id == $prod['Id']){
                    return true;
                }
            }
            return false;
        }
    }

    //Getting a product's sizes from the product detail
    public function getProductSizes($productID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_product_sizes = "SELECT * FROM productdetail WHERE ProductID = ? ORDER BY Size ASC";
        $statement = mysqli_stmt_init($connection);

        $sizes = array();

        if(mysqli_stmt_prepare($statement, $get_product_sizes)){
            mysqli_stmt_bind_param($statement, 'i', $productID);
            mysqli_stmt_execute($statement);
            $result = mysqli_stmt_get_result($statement);
            while($row = mysqli_fetch_assoc($result)){
                $sizes[] = $row;
            }
        }

        return $sizes;
    }

    //Get the sum of the product in the stock
    public function getProductSum($productID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_product_sizes = "SELECT SUM(Quantity) as quantities FROM productdetail WHERE ProductID = ?";
        $statement = mysqli_stmt_init($connection);

        if(mysqli_stmt_prepare($statement, $get_product_sizes)){
            mysqli_stmt_bind_param($statement, 'i', $productID);
            mysqli_stmt_execute($statement);
            $result = mysqli_stmt_get_result($statement);
            return $result->fetch_object()->quantities;
        }
    }

    //Get Product Quantity in stock
    public function getProductQuantity($productID, $Size){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_product_sizes = "SELECT Quantity FROM productdetail WHERE ProductID = ? AND Size = ?";
        $statement = mysqli_stmt_init($connection);

        if(mysqli_stmt_prepare($statement, $get_product_sizes)){
            mysqli_stmt_bind_param($statement, 'is', $productID, $Size);
            mysqli_stmt_execute($statement);
            $result = mysqli_stmt_get_result($statement);
            return $result->fetch_object()->Quantity;
        }
    }
    
}

?>