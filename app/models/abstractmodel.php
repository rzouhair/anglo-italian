<?php 
namespace ANGIT\Models;
class AbstractModel{
    public static function uploadFile($inputName, $uploadDestination){
        $files = $_FILES[$inputName];
        $uploadedFiles = "";
        foreach ($files['name'] as $position => $file) {
            if(count($files['name']) === 1){
                if($files['name'][0] === ""){
                    break;
                }
            }
            $fileName = $files['name'][$position];
            $fileTmpName = $files['tmp_name'][$position];
            $fileError = $files['error'][$position];
            $fileSize = $files['size'][$position];
            
            $fileExtension = explode('.' ,$fileName);
            $fileExtension = end($fileExtension);
            
            $allowedExt = array('jpg', 'png', 'jpeg');
            if(!in_array($fileExtension, $allowedExt)){
                echo 'Not Allowed';
                return;
            }else{
                if($fileError === 0){
                    if($fileSize <= 5242880){
                        $fileNewName = uniqid('', true) . '.' . $fileExtension;
                        $fileDestination = $uploadDestination . DS . $fileNewName;

                        if(move_uploaded_file($fileTmpName, $fileDestination)){
                            $uploadedFiles .= ($position === 0) ? $fileNewName : '|' . $fileNewName;
                        }else{
                            echo 'There was an error uploading your file';
                            return;
                        }
                    }else{
                        echo 'Your File is Too Big';
                    }
                }else{
                    echo 'There was an error';
                    return;
                }
            }
        }
        return $uploadedFiles;
    }


}


?>