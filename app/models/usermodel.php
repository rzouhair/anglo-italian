<?php 
namespace ANGIT\Models;
use ANGIT\Models\AbstractModel;

class UserModel extends AbstractModel{
    
    public $firstname;
    public $lastname;
    public $email;
    public $address;
    public $address2;
    public $country;
    public $city;
    public $phoneNumber;
    public $postalCode;
    public $password;
    public $question;
    public $answer;
    public $signupDate;

    public function __construct()
    {   
    }

    public static function constructor($firstname, $lastname, $email, $address,$address2, $country, $city, $phoneNumber, $postalCode, $password, $question, $answer){
        $instance = new self();
        $instance->firstname = $firstname;
        $instance->lastname = $lastname;
        $instance->email = $email;
        $instance->address = $address;
        $instance->address2 = $address2;
        $instance->country = $country;
        $instance->city = $city;
        $instance->phoneNumber = $phoneNumber;
        $instance->postalCode = $postalCode;
        $instance->password = $password;
        $instance->question = $question;
        $instance->answer = $answer;
        $instance->signupDate = date('Y-m-d');
        return $instance;
    }

    //Inserting a new user in the database
    public function InsertUser(){

        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $insert_user = 'CALL Insert_User(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
        $insert_user_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($insert_user_statement, $insert_user)){
            mysqli_stmt_bind_param($insert_user_statement, 'sssssissisiss', $this->firstname, $this->lastname, $this->email, $this->address, $this->address2, $this->country, $this->city, $this->phoneNumber, $this->postalCode, $this->password, $this->question, $this->answer, $this->signupDate);
            mysqli_stmt_execute($insert_user_statement);
            return true;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }

    }

    //Getting the User Infos By passing the ID as an argument
    public function GetUserInfosByID($ID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $get_user_infos = 'SELECT * FROM userinfos WHERE ID = ?';
        $get_user_infos_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_user_infos_statement, $get_user_infos)){
            mysqli_stmt_bind_param($get_user_infos_statement, 'i', $ID);
            mysqli_stmt_execute($get_user_infos_statement);
            $user_infos_result = mysqli_stmt_get_result($get_user_infos_statement);
            while($row = mysqli_fetch_assoc($user_infos_result)){
                return $row;
            }
        }
    }

    //Getting the user infos for sign in
    public function GetUserInfos($email){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $user_array = array();

        $select_user = 'SELECT * FROM userinfos WHERE Email = ?';
        $select_user_statement = mysqli_stmt_init($connection);
        
        if(mysqli_stmt_prepare($select_user_statement, $select_user)){
            mysqli_stmt_bind_param($select_user_statement, 's', $email);
            mysqli_stmt_execute($select_user_statement);
            $user_result = mysqli_stmt_get_result($select_user_statement);
            while($row = mysqli_fetch_assoc($user_result)){
                $user_array[] = $row;
            }
            return $user_array;
        }else{
            return 'notfounduser';
        }
    }

    //Checking for password correspondence
    public function CheckPassword($email, $password){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $selectPassword = "SELECT UserPassword FROM userinfos WHERE Email = ?";
        $select_password_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_password_statement, $selectPassword)){
            mysqli_stmt_bind_param($select_password_statement, 's', $email);
            mysqli_stmt_execute($select_password_statement);
            $result = mysqli_stmt_get_result($select_password_statement);
            $hashedPassword = $result->fetch_object()->UserPassword;
            return (password_verify($password, $hashedPassword)) ? true : false;
        }
    }

    //Change User password
    public function ChangePassword($userID, $oldPassword, $newPassword, $newPasswordConfirmation){
        if(strlen($oldPassword) == 0 || strlen($newPassword) == 0 || strlen($newPasswordConfirmation) == 0){
            return 'fieldsmissing';
        }else if($newPassword != $newPasswordConfirmation){
            return 'confirmpass';
        }else if($oldPassword == $newPassword){
            return 'samepass';
        }else{
            $userInfos = $this->GetUserInfosByID($userID);
            if(password_verify($oldPassword, $userInfos['UserPassword']) || $oldPassword == $userInfos['UserPassword']){
                
                $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

                $get_cart_id = 'UPDATE userinfos SET UserPassword = ? WHERE ID = ?';
                $get_cart_id_statement = mysqli_stmt_init($connection);
                if(mysqli_stmt_prepare($get_cart_id_statement, $get_cart_id)){
                    $hashedPassword = password_hash($newPasswordConfirmation, PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($get_cart_id_statement, 'si', $hashedPassword, $userID);
                    if(mysqli_stmt_execute($get_cart_id_statement)){
                        return true;
                    }else{
                        return 'errorocc';
                    }
                }
            }else{
                return 'wrongpass';
            }
        }
    }

    //Checking whether the input email exists or not
    public function checkEmailExistance($email){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_email = "SELECT Email FROM userinfos WHERE Email = ?";
        $select_email_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_email_statement, $select_email)){
            mysqli_stmt_bind_param($select_email_statement, 's', $email);
            mysqli_stmt_execute($select_email_statement);
            mysqli_stmt_store_result($select_email_statement);
            return (mysqli_stmt_num_rows($select_email_statement) > 0) ? true : false;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }

    }

    //Checking whether the input question exists or not
    public function checkQuestionExistance($questionID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_questions = "SELECT ID FROM questions WHERE ID = ?";
        $select_questions_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_questions_statement, $select_questions)){
            $questionID = filter_var($questionID, FILTER_SANITIZE_NUMBER_INT);
            mysqli_stmt_bind_param($select_questions_statement, 'i', $questionID);
            mysqli_stmt_execute($select_questions_statement);
            mysqli_stmt_store_result($select_questions_statement);
            return (mysqli_stmt_num_rows($select_questions_statement) > 0) ? true : false;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }

    }

    //Checking whether the input question exists or not
    public function checkCountryExistance($countryID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_countries = "SELECT id FROM countries WHERE id = ?";
        $select_countries_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_countries_statement, $select_countries)){
            mysqli_stmt_bind_param($select_countries_statement, 'i', $countryID);
            mysqli_stmt_execute($select_countries_statement);
            mysqli_stmt_store_result($select_countries_statement);
            return (mysqli_stmt_num_rows($select_countries_statement) > 0) ? true : false;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }

    }

    //Checking whether the input question exists or not
    public function getAllQuestions(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_questions = "SELECT * FROM questions";
        $select_questions_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_questions_statement, $select_questions)){
            mysqli_stmt_execute($select_questions_statement);
            $question_result = mysqli_stmt_get_result($select_questions_statement);
            return $question_result;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }

    }

    //Getting the user's First and Last Name
    public function getFullName($ID){
        $currentUserInfos = $this->GetUserInfosByID($ID);
        $firstName = $currentUserInfos['FirstName'];
        $lastname = $currentUserInfos['LastName'];
        return ucfirst(strtolower($firstName)) . ' ' . ucfirst(strtolower($lastname));
    } 

    //Getting All countries regisetred in the Db
    public function getAllCountries(){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $select_countries = "SELECT * FROM Countries";
        $select_countries_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_countries_statement, $select_countries)){
            mysqli_stmt_execute($select_countries_statement);
            $countries_result = mysqli_stmt_get_result($select_countries_statement);
            return $countries_result;
        }else{
            header('Location: user/join?error=servererror');
            exit();
        }
    }

    //Add Product to Bag/Cart
    public function addToBag($signedInUserID, $productID, $productSize){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $add_to_bag = 'CALL AddToCart(?, ?, ?)';
        $add_to_bag_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($add_to_bag_statement, $add_to_bag)){
            mysqli_stmt_bind_param($add_to_bag_statement, "iis", $signedInUserID, $productID, $productSize);
            mysqli_stmt_execute($add_to_bag_statement);
            return true;
        }
        return false;
    }

    //Checking whether the product exists in user's bag or not
    public function checkProductInBag($productID, $selectedSize, $UserID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $check_bag = 'CALL CheckProductInBag(?, ?, ?);';
        
        $check_bag_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($check_bag_statement, $check_bag)){
            mysqli_stmt_bind_param($check_bag_statement, 'iis', $UserID, $productID, $selectedSize);
            mysqli_stmt_execute($check_bag_statement);
            mysqli_stmt_store_result($check_bag_statement);
            $rowNum = mysqli_stmt_num_rows($check_bag_statement);
            return $rowNum;
        }
        
    }

    //Getting All Products In User's Bag
    public function getAllProductsInBag($userID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $my_products_array = array();

        $select_my_products = "CALL GetProductsInBag(?)";
        $select_my_products_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($select_my_products_statement, $select_my_products)){
            mysqli_stmt_bind_param($select_my_products_statement, 'i', $userID);
            mysqli_stmt_execute($select_my_products_statement);
            $my_products_result = mysqli_stmt_get_result($select_my_products_statement);
            while($row = mysqli_fetch_assoc($my_products_result)){
                $my_products_array[] = $row;
            }
            return $my_products_array;
        }
    }

    //Getting the total cart price according to the connected user
    public function getTotalCartPrice($UserID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');
        $totalPrice = array();

        $get_cart_total = 'SELECT Get_Total_Cart_Price_Func(?) as Total';
        $get_cart_total_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_cart_total_statement, $get_cart_total)){
            mysqli_stmt_bind_param($get_cart_total_statement, 'i', $UserID);
            mysqli_stmt_execute($get_cart_total_statement);
            $totalPriceResult = mysqli_stmt_get_result($get_cart_total_statement);
            while($row = mysqli_fetch_assoc($totalPriceResult)){
                $totalPrice[] = $row['Total'];
            }
            return $totalPrice[0];
        } 
    }

    //Getting the cartID by passing the userID 
    public function getCartIDbyUserID($UserID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_cart_id = 'SELECT ID FROM cart WHERE UserID = ?';
        $get_cart_id_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_cart_id_statement, $get_cart_id)){
            mysqli_stmt_bind_param($get_cart_id_statement, 'i', $UserID);
            mysqli_stmt_execute($get_cart_id_statement);
            $cartID = mysqli_stmt_get_result($get_cart_id_statement)->fetch_object()->ID;
            return $cartID;
        }
    }

    //Decrease the quantity
    public function DecreaseQuantity($CartID, $ProductID, $ProductSize){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_cart_id = 'CALL Decrease_Quantity(?, ?, ?)';
        $get_cart_id_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_cart_id_statement, $get_cart_id)){
            mysqli_stmt_bind_param($get_cart_id_statement, 'iis', $CartID, $ProductID, $ProductSize);
            mysqli_stmt_execute($get_cart_id_statement);
            $Done = mysqli_stmt_get_result($get_cart_id_statement)->fetch_object()->Done;
            return $Done;
        }
    }

    //Increase the quantity
    public function IncreaseQuantity($CartID, $ProductID, $ProductSize){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_cart_id = 'CALL Increase_Quantity(?, ?, ?)';
        $get_cart_id_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_cart_id_statement, $get_cart_id)){
            mysqli_stmt_bind_param($get_cart_id_statement, 'iis', $CartID, $ProductID, $ProductSize);
            mysqli_stmt_execute($get_cart_id_statement);
            $Done = mysqli_stmt_get_result($get_cart_id_statement)->fetch_object()->Done;
            return $Done;
        }
    }

    //Deleting the product from user's cart
    public function DeleteProduct($CartID, $ProductID, $ProductSize){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $delete_product_from_cart = 'DELETE FROM cartdetail WHERE CartID = ? AND ProductID = ? AND ProductSize = ?';
        $delete_product_from_cart_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($delete_product_from_cart_statement, $delete_product_from_cart)){
            mysqli_stmt_bind_param($delete_product_from_cart_statement, 'iis', $CartID, $ProductID, $ProductSize);
            mysqli_stmt_execute($delete_product_from_cart_statement);
        }
    }

    //Get user's bag products count
    public function GetBagCount($CartID){
        $connection = mysqli_connect('localhost', 'root', '', 'angloitalian');

        $get_products_count = 'SELECT SUM(ProductQuantity) AS BagCount FROM cartdetail WHERE CartID = ?';
        $get_products_count_statement = mysqli_stmt_init($connection);
        if(mysqli_stmt_prepare($get_products_count_statement, $get_products_count)){
            mysqli_stmt_bind_param($get_products_count_statement, 'i', $CartID);
            mysqli_stmt_execute($get_products_count_statement);
            $products_count_result = mysqli_stmt_get_result($get_products_count_statement);
            $count = $products_count_result->fetch_object()->BagCount;
            return $count;
        }
    } 
}