<?php 
namespace ANGIT;
if(!defined('DS')){
    define('DS', DIRECTORY_SEPARATOR);
}

define('APP_PATH', realpath(dirname(__FILE__)));
define('VIEWS_PATH', APP_PATH .DS. 'views');
define('TEMPLATES_PATH', APP_PATH .DS. 'templates');
define('MODELS_PATH', APP_PATH .DS. 'models');
define('INDEX_FILE_PATH', '..'.DS.'public'.DS.'index.php');
define('UPLOAD_DESTINATION_PATH', '..'.DS.'public'.DS.'images');
define('AJAX_PATH', APP_PATH . DS . 'ajax');
define('SHOP_MODEL', MODELS_PATH . DS . 'shopmodel.php');


define('DATABASE', APP_PATH . DS . 'lib' . DS . 'database.php');

define('NOT_FOUND_CONTROLLER', 'ANGIT\Controllers\NotFoundController');
define('NOT_FOUND_ACTION', 'notFoundAction');
?>
