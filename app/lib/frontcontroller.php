<?php 
namespace ANGIT\LIB;

use ANGIT\Models\ShopModel;

class FrontController{

    protected $controller = 'shop';
    protected $action = 'home';
    protected $params = array();
    
    public function __construct()
    {
        $this->ParseUrl();
    }

    public function ParseUrl(){
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', trim($url, '/'));
        
        if(isset($uri)){
            if(isset($uri[0]) && $uri[0] !== ''){
                $this->controller = $uri[0];
            }
    
            if(isset($uri[1]) && $uri[1] !== ''){
                $this->action = $uri[1];
            }
    
            for ($i=2; $i < count($uri); $i++) { 
                $this->params[] = $uri[$i];
            }
        }

    }

    public function dispatch(){
        $controllerClassName = 'ANGIT\Controllers' .DS. ucfirst($this->controller) . 'Controller';
        $actionName = $this->action . 'Action';
        if(!class_exists($controllerClassName) || $this->controller == 'ajax'){
            $controllerClassName = NOT_FOUND_CONTROLLER;
        }
        $_controller = new $controllerClassName();
        if(!method_exists($_controller, $actionName)){
            $this->action = $actionName = NOT_FOUND_ACTION;
        }
        $_controller->setController($this->controller);
        $_controller->setAction($this->action);
        $_controller->setParams($this->params);
        $_controller->$actionName();
    }
}

?>