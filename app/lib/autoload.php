<?php
namespace ANGIT\LIB;
abstract class AutoLoad{
    public static function autoload($className){
        $className = str_replace('ANGIT', '', $className);
        $className = strtolower($className) . '.php';
        $fullpath = APP_PATH . $className;
        if(file_exists($fullpath)){
            require_once($fullpath);
        }
        
    }
}

spl_autoload_register(__NAMESPACE__ . '\Autoload::autoload');