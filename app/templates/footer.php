    <footer>
        <?php if(!isset($_SESSION['isSignedIn'])) : ?>
            <form action="user/join" method="post" class="uk-flex uk-flex-center">
                <input type="text" name="footeremailaddress" id="emailadress" placeholder="Your Email Adress">
                <input type="submit" value="SIGN UP">
            </form>
        <?php else : ?>
            <h2>You're Signed In</h2>
        <?php endif; ?>    

        <div class="links uk-flex-first">
            <a href="/info/privacy">PRIVACY</a>
            <a href="/info/returns">RETURNS</a>
        </div>
        <div id="instagram" class="uk-flex uk-flex-middle">
            <span class="" uk-icon="icon: instagram; ratio: 1.3"></span>
            <a href="#" class="uk-margin-small-left">INSTAGRAM</a>
        </div>
        <br>
        <p>&copy; Anglo-Italian Clothing &copy; 2019</p>
    </footer>

    <script src="../js/angloitalian.js"></script>
</body>
</html>
