<?php 
use ANGIT\Models\UserModel;

if(session_status() == PHP_SESSION_NONE){
    session_start();
}

$header = ucfirst( (isset($this->params[0]) && !($this->action == 'product')) ? $this->params[0] : $this->action);
$css = $this->action;
$emptyUserInstance = new UserModel;
$Greeting_Message = (isset($_SESSION['SignedInUserID'])) ? $emptyUserInstance->getFullName($_SESSION['SignedInUserID']) : 'Not Signed In';
$CartID = (isset($_SESSION['SignedInUserID'])) ? $emptyUserInstance->getCartIDbyUserID($_SESSION['SignedInUserID']) : 0;
$count = ($emptyUserInstance->GetBagCount($CartID) == NULL) ? 0 : $emptyUserInstance->GetBagCount($CartID);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anglo Italian - <?php echo $header?></title>
    <script src="../js/jquery.js"></script>
    <script src="../js/jquery.zoom.js"></script>

    <script src="../js/uikit.js"></script>
    <script src="../js/uikit-icons.js"></script>
    <link rel="stylesheet" href="../css/uikit.css">
    <link rel="stylesheet" href="../css/navbar.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/<?php echo $css ?>.css">
    <link rel="shortcut icon" href="../images/Icon.png" type="image/x-icon">
</head>
<body>
    <!--Navbar-->
    <ul class="acco uk-margin-remove" uk-accordion>
        <li>
            <div class="uk-accordion-content uk-margin-remove">
                <div class="followSlider">
                    <form action="/user/join" method="post">
                        <label id="Greetings">Hello <span id="UserName"><?php echo $Greeting_Message ?></span> </label>
                        <div class="uk-inline">
                            <label> <a href="user/changePassword">Change Password</a> </label>
                        </div>
                        <span id="input">
                            <span uk-icon="sign-out">
                            <input type="submit" name="logout" value="Log Out">
                        </span>
                    </form>
                </div>
            </div>
        </li>
    </ul>
    
    <nav class="uk-navbar-container" uk-navbar>
        <div class="uk-navbar-left logo">
            <ul class="uk-navbar-nav">
                <li><a href="shop/home" class="logo"><img src="../images/Logo.svg" alt="svg"></a></li>
            </ul>
        </div>
        
        <div class="uk-navbar-right">
            <ul class="uk-navbar-nav uk-visible@m">
                <li><a href="shop/home">Shop</a></li>
                <li><a href="info/about">About</a></li>
                <li><a href="info/store">The Store</a></li>
                <?php 
                    if(!isset($_SESSION['isSignedIn'])){
                        echo '<li ><a href="user/join">Join</a></li>';
                    }else{
                        echo '<li id="follow"><a>Account</a></li>';
                    }
                ?>
                <?php if(isset($_SESSION['isSignedIn'])) : ?>
                    <li><a id="bag" class="Bag" href="shop/bag">Bag [<?php echo $count ?>]</a></li>
                <?php endif; ?>
                <li><a href="#searchModal" uk-toggle>Search</a></li>
            </ul>
        </div>
        <ul class="Bag uk-hidden@m">
        </ul>
        
        <!--Offcanvas Navigation Bar-->
        <div class="uk-inline uk-hidden@m uk-float-right">  
            <div class="offcanvas">
                <a class="uk-navbar-toggle uk-padding-remove uk-margin-remove ocButton" uk-toggle="target: #ocnavbar">MENU</a>
                <div id="ocnavbar" uk-offcanvas="overlay: true; mode: slide; flip: true">
                    <div class="uk-offcanvas-bar">
                        <ul class="uk-list uk-list-divider uk-list-large">
                            <li><a href="shop/home" class="uk-button uk-text-bold">Shop</a></li>
                            <li><a href="info/store" class="uk-button uk-text-bold">The Store</a></li>
                            <li><a href="info/about" class="uk-button uk-text-bold">About</a></li>
                            <?php if(isset($_SESSION['isSignedIn'])) : ?>
                                <li><a class="Bag uk-button uk-text-bold" href="shop/bag">Bag</a></li>
                            <?php endif; ?>
                            <li><a href="#searchModal" class="uk-button uk-text-bold" uk-toggle>Search</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
    </nav>
   
    <!--/Navbar-->
    
    <div id="searchModal" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog" uk-height-viewport>
            <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
            <div class="uk-padding-large">
                <form method="post" class="uk-search uk-search-large">
                    
                    <span id="searchIcon" uk-search-icon></span>
                    <input class="searchInput uk-search-input" maxlength="100" type="search" placeholder="Search...">
                </form>

                <div class="resultsContainer uk-flex uk-flex-around uk-flex-middle uk-child-width-1-3@m uk-child-width-1-2@s uk-child-width-1-1" uk-grid>
                    
                </div>
            </div>
        </div>
    </div>